<?php
namespace App\Http\Validation;

class Blocks
{

    public static $rules = [
        'name' => 'required|max:254'
    ];

    public static function messages()
    {
        return [
            'name' => [
                'validation.required' => 'Pole "Nazwa bloku" jest wymagane'
            ],
        ];
    }
}
