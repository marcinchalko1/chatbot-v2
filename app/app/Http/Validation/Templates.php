<?php
namespace App\Http\Validation;

class Templates
{

    public static $rules = [
        'name' => 'required|max:254'
    ];

    public static function messages()
    {
        return [
            'name' => [
                'validation.required' => 'Pole "Nazwa szablonu" jest wymagane'
            ],
        ];
    }
}
