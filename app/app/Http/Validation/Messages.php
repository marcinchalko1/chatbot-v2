<?php
namespace App\Http\Validation;

class Messages
{

    public static $rules = [
        'leadId' => 'required|max:1000',
        'campaignId' => 'required|max:1000',
        'conversationId' => 'required|max:1000',
        'count' => 'required|max:1000',
        'content' => 'required|max:1000',
    ];

}
