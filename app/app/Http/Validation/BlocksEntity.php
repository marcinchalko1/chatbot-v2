<?php
namespace App\Http\Validation;

class BlocksEntity
{

    public static $rules = [
        'blocks'    => 'required|array|min:3',
        'blocks.*'  => 'required|string|distinct',
    ];

    public static function messages()
    {
        return [
            'blocks' => [
                'validation.required' => 'Pole "Nazwa bloku" jest wymagane'
            ],
        ];
    }
}
