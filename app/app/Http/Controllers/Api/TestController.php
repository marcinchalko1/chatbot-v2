<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ixudra\Curl\Facades\Curl;

class TestController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth');
    }
    
    public function host(Request $request)
    {
        print $request->server('HTTP_HOST');
    }

    public function get(Request $request)
    {
        return Curl::to('https://chalko.eu/chatbot/app/public/api/product/id/1')
                ->withHeader('Authorization: Bearer WLonKahKVz5GbmSfgnniblkqRGYrYiwnMa8bHMoBPA60aYWGN2WI9F6fO61B')
                ->asJsonRequest()
                ->get();
    }

    public function createMessage(Request $request)
    {
//        $request->server('HTTP_HOST')
        return Curl::to('https://chalko.eu/api/messages')
            ->withHeader('Authorization: Bearer WLonKahKVz5GbmSfgnniblkqRGYrYiwnMa8bHMoBPA60aYWGN2WI9F6fO61B')
            ->withHeader('Content-type: application/json')
            ->withHeader('Accept: application/json')
            ->withData( 
                array( 
                    'leadId' => 'leadId',
                    'campaignId' => 'campaignId',
                    'conversationId' => 'conversationId',
                    'count' => 'count',
                    'content' => 'content',
                ) 
            )
            ->post();
    }
}
