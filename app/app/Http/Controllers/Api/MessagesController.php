<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Services\VerfifyApiToken;
use App\Http\Validation\Messages;
use App\Repositories\Messages\Repository as MessagesRepository;

class MessagesController extends Controller
{

    public function __construct(Request $request)
    {
//        VerfifyApiToken::verify($request);
    }

    public function create(Request $request)
    {
        $validatedData = $request->validate(Messages::$rules);

        MessagesRepository::create($validatedData);

        return view('api.messages.create');
    }
}
