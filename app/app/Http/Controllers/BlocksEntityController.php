<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Validation\BlocksEntity as BlocksEntityValidation;
use App\Repositories\BlocksEntity\Repository as BlocksEntityRepository;
use App\Repositories\Templates\Repository as TemplatesRepository;
use App\Services\CreateBlocksEntity;
use App\Services\CreateScreenplay;

class BlocksEntityController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Add blocks entity.
     * 
     * @param  Illuminate\Http\Request  $request
     * @return string
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post') && $request->templateId && $request->blockId) {
            
            $messages = NULL;
            $buttons = NULL;
            $switchToBlock = NULL;
            
            foreach ($request->input('blocks') as $blocksItem) {
                foreach ($blocksItem as $blocksItemItem) {
                    switch ($blocksItemItem['type']) {
                        case 1:
                            $messages[] = $blocksItemItem;
                            break;
                        case 2:
                            $buttons[] = $blocksItemItem;
                            break;
                        case 3:
                            $switchToBlock[] = $blocksItemItem;
                            break;
                        case 4:
                            $messages[] = $blocksItemItem;
                            break;
                        case 5:
                            $messages[] = $blocksItemItem;
                            break;
                        case 6:
                            $messages[] = $blocksItemItem;
                            break;
                    }
                }
            }
            
//            foreach ($messages as $messagesItem) { 
//                echo '<pre>' . print_r($messagesItem) . '</pre>';
//            }
            
//            echo '<pre>' . print_r($messages) . '</pre>';
            
//            die;
            
//            $buttons = (! empty($request->input('blocks')[2])) ? $request->input('blocks')[2] : NULL;
//            $switchToBlock = (! empty($request->input('blocks')[3])) ? $request->input('blocks')[3] : NULL;
            $blockId = $request->blockId;
            
            $createBlocksEntity = new CreateBlocksEntity;
            $createBlocksEntity->setMessages($messages);
            $createBlocksEntity->setButtons($buttons);
            $createBlocksEntity->setSwitchToBlock($switchToBlock);
            $createBlocksEntity->setBlockId($blockId);
            $createBlocksEntity->create();
            
            if (TemplatesRepository::isPublish($request->templateId)) {
                $createScreenplay = new CreateScreenplay(2);
                $createScreenplay->generate($request);
            }
            
            return redirect(
                'templates/form/'.$request->templateId.'/'.$request->blockId
            );
        }
    }
}
