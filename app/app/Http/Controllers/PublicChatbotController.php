<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CreateScreenplay;

class PublicChatbotController extends Controller
{

    public $campaignId = 1;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show testing chatbot page.
     *
     * @return \Illuminate\Http\Response
     */
    public function production(Request $request)
    {
        $createScreenplay = new CreateScreenplay(2);
        $createScreenplay->generate($request);
        
        return redirect('/home');
    }
}
