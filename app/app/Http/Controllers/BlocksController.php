<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Validation\Blocks as BlocksValidation;
use App\Repositories\Blocks\Repository as BlocksRepository;
use App\Repositories\BlocksEntity\Repository as BlocksEntityRepository;
use App\Resources\Blocks as BlocksResources;
use App\Services\CreateBlocksEntity;

class BlocksController extends Controller
{

    private $defaultBlocks = [
        1 => 'getEnd',
        2 => 'getByeBye',
        3 => 'getDate',
        4 => 'getHour',
        5 => 'getConfirm',
        6 => 'getEmail'
    ];
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create new template.
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post') && $request->templateId) {
            $validatedData = $request->validate(BlocksValidation::$rules);
            $blockId = BlocksRepository::create($validatedData, $request->templateId);
            return redirect('templates/form/'.$request->templateId.'/'.$blockId);
        }
    }

    /**
     * Update template.
     */
    public function update(Request $request)
    {
        if ($request->isMethod('post') && $request->templateId && $request->blockId) {
            $validatedData = $request->validate(BlocksValidation::$rules);
            $blockId = BlocksRepository::update(
                $request->blockId, 
                $validatedData['name']
            );
            return redirect('templates/form/'.$request->templateId.'/'.$blockId);
        }
    }

    /**
     * Delete block.
     */
    public function delete(Request $request)
    {
        if ($request->blockId && $request->templateId) {
            BlocksEntityRepository::delete($request->blockId);
            BlocksRepository::delete($request->blockId);
            return redirect('templates/form/'.$request->templateId);
        }
    }
    
    /**
     * Create default block.
     */
    public function defaults(Request $request)
    {
        if ($request->templateId && $request->defaultBlockId) {
            
            $method = $this->defaultBlocks[$request->defaultBlockId];
            $data = BlocksResources::$method();
//            echo print_r($data); die;
            
            $blockData['name'] = $data['name'];
            $blockId = BlocksRepository::create($blockData, $request->templateId);
            
            $messages = (! empty($data['blocks'][1])) ? $data['blocks'][1] : NULL;
            $buttons = (! empty($data['blocks'][2])) ? $data['blocks'][2] : NULL;
            $createBlocksEntity = new CreateBlocksEntity;
            $createBlocksEntity->setMessages($messages);
            $createBlocksEntity->setButtons($buttons);
            $createBlocksEntity->setBlockId($blockId);
            $createBlocksEntity->create();
            
            return redirect('templates/form/'.$request->templateId.'/'.$blockId);
        }
    }
}
