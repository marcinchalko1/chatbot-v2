<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Validation\Templates as TemplatesValidation;
use App\Repositories\Templates\Repository as TemplatesRepository;
use App\Repositories\Blocks\Repository as BlocksRepository;
use App\Repositories\BlocksEntity\Repository as BlocksEntityRepository;

class TemplatesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the template form.
     *
     * @return \Illuminate\Http\Response
     */
    public function form(Request $request)
    {
        $templateData = TemplatesRepository::getTemplate($request->templateId);

        $blocksData = BlocksRepository::getBlocksByTemplateId(
            $request->templateId
        );

        if (! $request->blockId) {
            $blockData = BlocksRepository::getFirstBlockByTemplateId(
                $request->templateId
            );
        } else {
            $blockData = BlocksRepository::getBlock($request->blockId);
        }
        
        $blockId = ($request->blockId) ? $blockData->block_id : 0;
        $blockName = ($request->blockId) ? $blockData->name : NULL;
        
        $blocksEntity = BlocksEntityRepository::getBlocksEntityByBlockId(
            $blockId
        );

        return view('templates.form', [
            'pageName' => 'Szablon',
            'templateName' => $templateData->name,
            'templateId' => $templateData->template_id,
            'blocks' => $blocksData,
            'blockId' => $blockId,
            'blockName' => $blockName,
            'blocksEntity' => $blocksEntity,
            'validation' => TemplatesValidation::messages(),
            'breadcrumb' => [
                [
                    'class' => '',
                    'href' => '/home',
                    'value' => 'Dashboard'
                ],
                [
                    'class' => 'active',
                    'href' => '',
                    'value' => 'Szablon: ' . $templateData->name
                ]
            ]
        ]);
    }

    /**
     * Form new template.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('templates.create', [
            'pageName' => 'Szablon',
            'validation' => TemplatesValidation::messages(),
            'breadcrumb' => [
                ['class' => 'active', 'href' => 'home', 'value' => 'Szablon']
            ]
        ]);
    }

    /**
     * Create new template.
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            $validatedData = $request->validate(TemplatesValidation::$rules);
            $templateId = TemplatesRepository::create($validatedData);
            BlocksRepository::createDefault($templateId);
            return redirect('templates/form/' . $templateId);
        }
    }

    /**
     * Update template.
     */
    public function update(Request $request)
    {
        if ($request->isMethod('post') && $request->templateId) {
            $validatedData = $request->validate(TemplatesValidation::$rules);
            $templateId = TemplatesRepository::update(
                $request->templateId, 
                $validatedData['name']
            );
            return redirect('templates/form/' . $templateId);
        }
    }

    /**
     * Delete template.
     */
    public function delete(Request $request)
    {
        if ($request->templateId) {
            $blocks = BlocksRepository::getBlocksByTemplateId($request->templateId);
            foreach ($blocks as $blocksItem) {
                BlocksEntityRepository::delete($blocksItem->block_id);
                BlocksRepository::delete($blocksItem->block_id);
            }
            TemplatesRepository::delete($request->templateId);
            return redirect('/home');
        }
    }
}
