<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Repositories\Templates\Repository as TemplatesRepository;

class HomeController extends Controller
{

    public $campaignId = 1;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $templates = TemplatesRepository::getTemplates();
        
        return view('home', [
            'pageName' => 'Dashboard',
            'templates' => $templates,
            'breadcrumb' => [
                ['class' => 'active', 'href' => 'home', 'value' => 'Dashboard']
            ]
        ]);
    }
}
