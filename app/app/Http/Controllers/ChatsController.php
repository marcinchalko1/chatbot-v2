<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Repositories\Messages\Repository as MessagesRepository;

class ChatsController extends Controller
{

    public $campaignId = 1;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $minCreatedAt = MessagesRepository::getMinCreatedAt();        
        $dateFrom = ($request->dateFrom) ? $request->dateFrom : $minCreatedAt;
        $dateTo = ($request->dateTo) ? $request->dateTo : date("Y-m-d H:i:s");

        $leads = MessagesRepository::countLeads();
        $conversations = MessagesRepository::countConversations();
        $countConversationsWithAnswers = MessagesRepository::countConversationsWithAnswers();
        $getConversationsWithAnswers = MessagesRepository::getConversationsWithAnswers(
            $dateFrom, 
            $dateTo
        );
        $getConversationsOnTrue = MessagesRepository::getConversationsOnTrue();
        $getConversationsOnFalse = MessagesRepository::getConversationsOnFalse();

        return view('chats', [
            'pageName' => 'Chats',
            'breadcrumb' => [
                ['class' => 'active', 'href' => 'chats', 'value' => 'Chats']
            ],
            'filter' => [
                'dateFrom' => date("Y-m-d", strtotime($dateFrom)),
                'dateTo' => date("Y-m-d", strtotime($dateTo))
            ],
            'leads' => $leads,
            'conversations' => $conversations,
            'countConversationsWithAnswers' => $countConversationsWithAnswers,
            'getConversationsWithAnswers' => $getConversationsWithAnswers,
            'getConversationsWithAnswers' => $getConversationsWithAnswers->appends(
                Input::except('page')
            ),            
            'getConversationsOnTrue' => $getConversationsOnTrue,
            'getConversationsOnFalse' => $getConversationsOnFalse
        ]);
    }

    /**
     * Show all messages in conversation with chatbot.
     *
     * @return \Illuminate\Http\Response
     */
    public function messages(Request $request)
    {
        $messages = MessagesRepository::getMessages($request->leadId, $request->conversationId);

        return view('home.messages', [
            'messages' => $messages,
        ]);
    }
}
