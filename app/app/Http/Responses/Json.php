<?php
namespace App\Http\Responses;

class Json
{

    public static function get($data)
    {
        return response()->json($data);
    }
}
