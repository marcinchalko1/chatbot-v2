<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Blocks extends Model
{
    protected $fillable = ['block_id', 'template_id', 'name', 'created_at', 'updated_at'];
    protected $table = 'blocks';
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

}
