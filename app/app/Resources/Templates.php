<?php
namespace App\Resources;

class Templates
{

    protected static $data;

    protected static function getObject()
    {
        return (object) [
                'template_id' => static::$data['template_id'],
                'name' => static::$data['name'],
                'created_at' => static::$data['created_at'],
                'updated_at' => static::$data['updated_at']
        ];
    }
}
