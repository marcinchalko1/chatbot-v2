<?php
namespace App\Resources;

class User
{

    protected static $data;

    protected static function getObject()
    {
        return (object) [
                'id' => static::$data['id'],
                'name' => static::$data['name'],
                'email' => static::$data['email'],
                'password' => static::$data['password'],
                'remember_token' => static::$data['remember_token'],
                'created_at' => static::$data['created_at'],
                'updated_at' => static::$data['updated_at'],
                'api_token' => static::$data['api_token'],
        ];
    }
}
