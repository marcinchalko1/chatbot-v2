<?php
namespace App\Resources;

class Blocks
{

    protected static $data;

    protected static function getObject()
    {
        return (object) [
                'block_id' => static::$data['block_id'],
                'template_id' => static::$data['template_id'],
                'name' => static::$data['name'],
                'created_at' => static::$data['created_at'],
                'updated_at' => static::$data['updated_at']
        ];
    }
    
    public static function getEnd()
    {
        return [
            'name' => 'end',
            'blocks' => [
                2 => [
                    [
                        'type' => 2,
                        'sequence' => 1,
                        'parametrs' => [
                            'action' => [
                                'text' => 'Bardzo dziękuję!',
                                'value' => 'trueend'
                            ],
                            'delay' => 0
                        ]
                    ]
                ]
            ]
        ];
    }
    
    public static function getByeBye()
    {
        return [
            'name' => 'byebye',
            'blocks' => [
                1 => [
                    [
                        'type' => 1,
                        'sequence' => 1,
                        'parametrs' => [
                            'content' => 'Dziękuję za odpowiedź.',
                            'delay' => 0,
                            'loading' => 'on'
                        ]
                    ],
                    [
                        'type' => 1,
                        'sequence' => 1,
                        'parametrs' => [
                            'content' => 'Pozdrawiam serdecznie i mam nadzieję — do zobaczenia.',
                            'delay' => 3000,
                            'loading' => 'on'
                        ]
                    ],
                    
                ]
            ]
        ];
    }
}
