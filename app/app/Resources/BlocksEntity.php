<?php
namespace App\Resources;

class BlocksEntity
{

    protected static $data;
    private static $dataFormat = 'Y-m-d H:i:s';

    protected static function getObject()
    {
        return (object) [
                'block_entity_id' => static::$data['block_id'],
                'block_id' => static::$data['block_id'],
                'parametrs' => static::$data['name'],
                'created_at' => static::$data['created_at'],
                'updated_at' => static::$data['updated_at']
        ];
    }
    
    public static function getDefaultBlocksEntity(int $blockId = NULL)
    {
        $createdAt = $updatedAt = date(static::$dataFormat);
        
        $firstBlockEntity = [
            'delay' => 3000,
            'loading' => true,
            'content' => 'Witaj'
        ];
        
        $secondBlockEntity = [
            'delay' => 0,
            'action' => [
                'text' => 'Witaj',
                'value' => NULL
            ]
        ];
            
        return [
            [
                'block_id' => $blockId,
                'type' => 1,
                'parametrs' => json_encode($firstBlockEntity),
                'created_at' => $createdAt,
                'updated_at' => $updatedAt
            ], [
                'block_id' => $blockId,
                'type' => 2,
                'parametrs' => json_encode($secondBlockEntity),
                'created_at' => $createdAt,
                'updated_at' => $updatedAt
            ]
        ];
    }
}
