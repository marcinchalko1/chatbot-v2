<?php
namespace App\Resources\TestChatbot;

use App\Resources\TestChatbot\Block;
use Illuminate\Database\Eloquent\Collection as BlocksEntity;
use App\Resources\TestChatbot\Block as TestingChatbotBlock;

class Template extends Block
{

    const TYPE_BLOCK_ENTITY_MESSAGE = 1;
    const TYPE_BLOCK_ENTITY_BUTTON = 2;    
    const TYPE_BLOCK_ENTITY_SWITCH_TO_BLOCK = 3;
    const TYPE_BLOCK_ENTITY_INPUT = 4;
    const TYPE_BLOCK_ENTITY_LINK= 5;
    const TYPE_BLOCK_ENTITY_IMAGE = 6;

    private $content = NULL;
    private $firstBlockName = NULL;

    public function __construct()
    {
        $this->content .= "var botui = new BotUI('bot');\n\n

function exdays() {\n
    return 365;\n
}\n\n

function setCookie(cname, cvalue, exdays) {\n
    var d = new Date();\n
    var cookiename = prefix() + cname;\n
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));\n
    var expires = \"expires=\" + d.toUTCString();\n
    document.cookie = cookiename + \"=\" + cvalue + \";\" + expires + \";path=/\";\n
}\n\n

function getCookie(cname) {\n
    var name = prefix() + cname + \"=\";\n
    var decodedCookie = decodeURIComponent(document.cookie);\n
    var ca = decodedCookie.split(';');\n
    for (var i = 0; i < ca.length; i++) {\n
        var c = ca[i];\n
        while (c.charAt(0) == ' ') {\n
            c = c.substring(1);\n
        }\n
        if (c.indexOf(name) == 0) {\n
            return c.substring(name.length, c.length);\n
        }\n
    }\n
    return \"\";\n
}\n\n

// SCENARIUSZ\n\n";
    }

    public function setBlock(
        int $sequence = 0, 
        string $name = NULL, 
        BlocksEntity $blocks
    ) {
        if(!empty($blocks[0]->type)) {
            
            $content = [];
            $buttons = [];
            
            $this->content .= "var $name = function () { \n\n";
            
            foreach ($blocks as $blockItem) {
                
                $params = json_decode($blockItem->parametrs);
                
                switch ($blockItem->type) {
                    
                    case self::TYPE_BLOCK_ENTITY_MESSAGE:                        
                        $content[] = [
                            'type' => self::TYPE_BLOCK_ENTITY_MESSAGE,
                            'params' => $params
                        ];
                        break;
                    
                    case self::TYPE_BLOCK_ENTITY_BUTTON:
                        $buttons[] = $params;
                        break;
                    
                    case self::TYPE_BLOCK_ENTITY_SWITCH_TO_BLOCK:
                        $switchToBlock[] = $params;
                        break;
                    
                    case self::TYPE_BLOCK_ENTITY_INPUT:
                        $content[] = [
                            'type' => self::TYPE_BLOCK_ENTITY_INPUT,
                            'params' => $params
                        ];
                        break;
                    
                    case self::TYPE_BLOCK_ENTITY_LINK:
                        $content[] = [
                            'type' => self::TYPE_BLOCK_ENTITY_LINK,
                            'params' => $params
                        ];
                        break;
                    
                    case self::TYPE_BLOCK_ENTITY_IMAGE:
                        $content[] = [
                            'type' => self::TYPE_BLOCK_ENTITY_IMAGE,
                            'params' => $params
                        ];
                        break;
                }
            } 
            
            $block = new TestingChatbotBlock;
            
            if (!$sequence) {
                $this->firstBlockName = $name;
            }
            
            $count = 0; 
            
            foreach ($content as $contentItem) {
                
                $typeEntity = $contentItem['type'];
                $contentEntityItem = $contentItem['params'];
                    
                switch ($typeEntity) {

                    case self::TYPE_BLOCK_ENTITY_MESSAGE:
                        $this->content .= $block->addMessage(
                            $count, 
                            $contentEntityItem
                        );
                        break;

                    case self::TYPE_BLOCK_ENTITY_INPUT:
                        $this->content .= $block->addInput(
                            $count, 
                            $contentEntityItem
                        );
                        break;

                    case self::TYPE_BLOCK_ENTITY_LINK:
                        $this->content .= $block->addLink(
                            $count, 
                            $contentEntityItem
                        );
                        break;

                    case self::TYPE_BLOCK_ENTITY_IMAGE:
                        $this->content .= $block->addImage(
                            $count, 
                            $contentEntityItem
                        );
                        break;

                }
                $count++;
            } 
            
            if (!empty($buttons)) {
                $this->content .= $block->addButtons($count, $buttons);
                $this->content .= $block->addSwitch($buttons);
            }
            
            if (! empty($switchToBlock)) {
                $this->content .= $block->addSwitchToBlock($switchToBlock);
            }
            
            $this->content .= "     });\n};\n\n";
        }
    }

    public function __call($method, $arguments)
    {
        $this->content .= call_user_func_array(array($this, $method), $arguments);
        $this->content .= "});\n\n";
    }
    
    public function get()
    {
        $this->content .=  "
$('#startConversation').click(function () {\n
    ".$this->firstBlockName."();\n
    $('#avatarImg').show();\n
});\n";

        return $this->content;
    }
}
