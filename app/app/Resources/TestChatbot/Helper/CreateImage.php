<?php
namespace App\Resources\TestChatbot\Helper;

class CreateImage
{
    
    private $alt = null;
    private $src = null;
    private $content = null;
    
    public function __construct(string $alt = null, string $src = null) 
    {
        $this->setAlt($alt);
        $this->setSrc($src);
    }
    
    private function setAlt(string $alt = null)
    {
        $this->alt = $alt;
    }
    
    private function setSrc(string $src = null)
    {
        $this->src = $src;
    }
    
    public function build()
    {
        $this->content = $this->getAlt();
        $this->content .= $this->getSrc();
    }
    
    private function getAlt()
    {
        return '!['.$this->alt.']';
    }
    
    private function getSrc()
    {
        return '('.$this->src.')';
    }
    
    public function get()
    {
        return $this->content;
    }
}
