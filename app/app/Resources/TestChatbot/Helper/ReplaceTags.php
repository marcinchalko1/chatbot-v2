<?php
namespace App\Resources\TestChatbot\Helper;

class ReplaceTags
{
    
    private $initialTag = "{{";
    private $finalTag = "}}";
    private $content = NULL;
    private $replace = NULL;
    private $find = NULL;
    
    public function __construct(string $content = NULL)
    {
        $this->setContent($content);
    }

    public function setContent(string $content = NULL)
    {
        $this->content = $content;
    }
    
    public function getPattern()
    {
        return '/'.$this->initialTag.'(.*?)'.$this->finalTag.'/s';
    }

    public function getTagsCollection()
    {
        preg_match_all($this->getPattern(), $this->content, $matches);
        
        return $matches[1];
    }
    
    public function createFindCollection()
    {        
        $tags = $this->getTagsCollection();
        
        foreach ($tags as $tagsItem) {
            
            $this->find[] = $this->initialTag.$tagsItem.$this->finalTag;
        }
    }
    
    public function createReplaceCollection()
    {
        $tags = $this->getTagsCollection();
        
        foreach ($tags as $tagsItem) {
            
            $this->replace[] = "' + getCookie('".trim($tagsItem)."') + '";
        }
    }
    
    public function replaceTags()
    {
        $this->createFindCollection();
        $this->createReplaceCollection();
        
        return str_replace($this->find, $this->replace, $this->content);
    }
}
