<?php
namespace App\Resources\TestChatbot\Helper;

class CreateLink
{
    
    private $href = null;
    private $text = null;
    private $blank = false;
    private $content = null;
    
    public function __construct(
        string $href = null, 
        string $text = null, 
        bool $blank = false
    ) {
        
        $this->setHref($href);
        $this->setText($text);
        $this->setBlank($blank);
    }
    
    private function setHref(string $href = null)
    {
        $this->href = $href;
    }
    
    private function setText(string $text = null)
    {
        $this->text = $text;
    }
    
    private function setBlank(string $blank = null)
    {
        $this->blank = $blank;
    }
    
    public function build()
    {
        $this->content = $this->getText();
        $this->content .= $this->getHref();
        $this->content .= $this->getBlank();
    }
    
    private function getHref()
    {
        return '('.$this->href.')';
    }
    
    private function getText()
    {
        return '['.$this->text.']';
    }
    
    private function getBlank()
    {
        return ($this->blank) ? '^' : NULL ;
    }
    
    public function get()
    {
        return $this->content;
    }
}
