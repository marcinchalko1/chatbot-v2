<?php
namespace App\Resources\TestChatbot;

use App\Resources\TestChatbot\Helper\ReplaceTags;
use App\Resources\TestChatbot\Helper\CreateLink;
use App\Resources\TestChatbot\Helper\CreateImage;

class BlockEntity
{
    
    public function addFirstButtons($params = NULL)
    {
        $buttons = "    
    botui.action.button({// let user choose something\r\n
        action: [";
        
foreach ($params as $paramsItem) {
    $buttons .= "
        {\n
            text: '".$paramsItem->action->text."',\n
            value: '".$paramsItem->action->value."'\n
        },\n";
}
        
        $buttons .= "
            ]\n";
        
        return $buttons;
    }

    public function addNextButtons($params = NULL)
    {
        $buttons = "    
    }).then(function () {\n
        return botui.action.button({// let user choose something\n
            action: [";
        
foreach ($params as $paramsItem) {
        $buttons .= "
            {\n
                text: '".$paramsItem->action->text."',\n
                value: '".$paramsItem->action->value."'\n
            },\n";
}

        $buttons .= "
            ]\n});\n";
        
        return $buttons;
    }
    
    
    public function addFirstInput($params = NULL)
    {
        $buttons = "    
    botui.action.text({// let user choose something\r\n
        action: [";
        
//foreach ($params as $paramsItem) {
    $buttons .= "
        {\n
            sub_type: '".$params->action->sub_type."',\n
            placeholder: '".$params->action->placeholder."'\n
        },\n";
//}
        
        $buttons .= "
            ]\n";
        
        return $buttons;
    }

    public function addNextInput($params = NULL)
    {
        
        $buttons = "    
    }).then(function () {\n
        return botui.action.text({// let user choose something\n
            action: [";
        
//foreach ($params as $paramsItem) {
        $buttons .= "
            {\n
                sub_type: '".$params->action->sub_type."',\n
                placeholder: '".$params->action->placeholder."'\n
            },\n";
//}

        $buttons .= "
            ]\n});\n
    }).then(function (res) {
        setCookie('".$params->action->sub_type."', res.value, exdays());";
        
        return $buttons;
    }

    public function addFirstMessage($params = NULL)
    {
        $replaceTags = new ReplaceTags($params->content);
        
        $loading = ($params->loading == 'on') ? 'true' : 'false';
        
        return "    
    botui.message.add({// show first message\r\n
        delay: ".$params->delay.",\n
        loading: $loading // fake typing\n
    }).then(function (index) {\n
        var content = '".$replaceTags->replaceTags()."';\n
        return botui.message.update(index, {// show first message\n
            content: content\n
        });\n
    ";
    }

    public function addNextMessage($params = NULL)
    {
        $replaceTags = new ReplaceTags($params->content); 
        
        $loading = ($params->loading == 'on') ? 'true' : 'false';
        
        return "    
    }).then(function () {\n
        return botui.message.add({\n
            delay: ".$params->delay.",\n
            loading: $loading\n
        });\n
    }).then(function (index) {\n
        var content = '".$replaceTags->replaceTags()."';\n
        return botui.message.update(index, {// show first message\n
            content: content\n
        });\n";
    }

    public function addFirstLink($params = NULL)
    {
        $createLink = new CreateLink(
            $params->href, 
            $params->text, 
            $params->blank
        ); 
        $createLink->build();
        
        $loading = ($params->loading == 'on') ? 'true' : 'false';
        
        return "    
    botui.message.add({// show first message\r\n
        delay: ".$params->delay.",\n
        loading: $loading // fake typing\n
    }).then(function (index) {\n
        var content = '".$createLink->get()."';\n
        return botui.message.update(index, {// show first message\n
            content: content\n
        });\n
    ";
    }

    public function addNextLink($params = NULL)
    {
        $createLink = new CreateLink(
            $params->href, 
            $params->text, 
            $params->blank
        ); 
        $createLink->build();
        
        $loading = ($params->loading == 'on') ? 'true' : 'false';
        
        return "    
    }).then(function () {\n
        return botui.message.add({\n
            delay: ".$params->delay.",\n
            loading: $loading\n
        });\n
    }).then(function (index) {\n
        var content = '".$createLink->get()."';\n
        return botui.message.update(index, {// show first message\n
            content: content\n
        });\n";
    }

    public function addFirstImage($params = NULL)
    {
        $createImage = new CreateImage($params->alt, $params->src);
        $createImage->build();
        
        $loading = ($params->loading == 'on') ? 'true' : 'false';
        
        return "    
    botui.message.add({// show first message\r\n
        delay: ".$params->delay.",\n
        loading: $loading // fake typing\n
    }).then(function (index) {\n
        var content = '".$createImage->get()."';\n
        return botui.message.update(index, {// show first message\n
            content: content\n
        });\n
    ";
    }

    public function addNextImage($params = NULL)
    {
        $createImage = new CreateImage($params->alt, $params->src);
        $createImage->build();
        
        $loading = ($params->loading == 'on') ? 'true' : 'false';
        
        return "    
    }).then(function () {\n
        return botui.message.add({\n
            delay: ".$params->delay.",\n
            loading: $loading\n
        });\n
    }).then(function (index) {\n
        var content = '".$createImage->get()."';\n
        return botui.message.update(index, {// show first message\n
            content: content\n
        });\n";
    }

    public function addSwitch($params = NULL)
    {
        $buttons = "    
    }).then(function (res) {\n
        switch (res.value.toLowerCase()) {\n";
        
foreach ($params as $paramsItem) {
        $buttons .= "
            case '".$paramsItem->action->value."':\n
                ".$paramsItem->action->value."();\n
                break;\n";
}

        $buttons .= "
            }\n";
        
        return $buttons;
    }

    public function addSwitchToBlock($params = NULL)
    {
        $buttons = "    
    }).then(function (res) {\n";
        
    foreach ($params as $paramsItem) {
            $buttons .= $paramsItem->action->value."();\n";
    }
        
        return $buttons;
    }
}
