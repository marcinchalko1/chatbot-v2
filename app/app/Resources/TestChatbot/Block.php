<?php
namespace App\Resources\TestChatbot;

use App\Resources\TestChatbot\BlockEntity;

class Block
{

    private $block = NULL;
    
    public function __construct()
    {
        $this->block = new BlockEntity();
    }
    
    public function addButtons(int $count = 0, $params = NULL)
    {
        if ($count) {
            return $this->block->addNextButtons($params);            
        } else {
            return$this->block->addFirstButtons($params);
        }
    }
    
    public function addMessage(int $count = 0, $params = NULL)
    {
        if ($count) {
            return $this->block->addNextMessage($params); 
        } else {
            return $this->block->addFirstMessage($params);
        }
    }
    
    public function addInput(int $count = 0, $params = NULL)
    {
        if ($count) {
            return $this->block->addNextInput($params); 
        } else {
            return $this->block->addFirstInput($params);
        }
    }
    
    public function addLink(int $count = 0, $params = NULL)
    {
        if ($count) {
            return $this->block->addNextLink($params); 
        } else {
            return $this->block->addFirstLink($params);
        }
    }
    
    public function addImage(int $count = 0, $params = NULL)
    {
        if ($count) {
            return $this->block->addNextImage($params); 
        } else {
            return $this->block->addFirstImage($params);
        }
    }
    
    public function addSwitch($params = NULL)
    {
        return $this->block->addSwitch($params);
    }
    
    public function addSwitchToBlock($params = NULL)
    {
        return $this->block->addSwitchToBlock($params);
    }
}
