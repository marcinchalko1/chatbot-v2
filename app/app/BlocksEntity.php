<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BlocksEntity extends Model
{
    protected $fillable = [
        'block_entity_id', 
        'block_id', 
        'type', 
        'sequence', 
        'parametrs', 
        'created_at', 
        'updated_at'
    ];
    
    protected $table = 'blocks_entity';
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

}
