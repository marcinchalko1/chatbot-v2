<?php
namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\User\Repository as UserRepository;

class VerfifyApiToken
{

    public static $bearer = "Bearer ";

    public static function verify(Request $request)
    {
        $authorization = $request->header('Authorization');
        $token = str_replace(static::$bearer, "", $authorization);
        UserRepository::getByApiToken($token);
    }
}
