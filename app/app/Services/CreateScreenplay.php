<?php
namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\Templates\Repository as TemplatesRepository;
use App\Repositories\Blocks\Repository as BlocksRepository;
use App\Repositories\BlocksEntity\Repository as BlocksEntityRepository;
use App\Resources\TestChatbot\Template as TestingChatbotTemplate;
use Illuminate\Support\Facades\Storage;

class CreateScreenplay
{

    private $diskType = [
        1 => 'test_uploads',
        2 => 'public_uploads'
    ];
    private $disk = NULL;

    public function __construct(int $disk = 1)
    {
        $this->disk = $this->diskType[$disk];
    }

    public function generate(Request $request)
    {
        $blocksData = BlocksRepository::getBlocksByTemplateId(
                $request->templateId
        );

        if (!empty($blocksData)) {

            $template = new TestingChatbotTemplate();

            $count = 0;
            foreach ($blocksData as $blocksDataItem) {

                $blocksEntity = BlocksEntityRepository::getBlocksEntityByBlockId(
                        $blocksDataItem->block_id
                );
                $template->setBlock($count, $blocksDataItem->name, $blocksEntity);
                $count++;
            }
        }

        $path = '/' . $request->templateId . '.js';
        if (!Storage::disk($this->disk)->put($path, $template->get())) {
            return false;
        }

        if ($this->disk == 'public_uploads') {
            $this->generateView($request->templateId);
            $this->setPublish($request->templateId);
        }
    }

    private function generateView(int $templateId = 0)
    {
        $template = '
            <!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <link rel="shortcut icon" href="https://s3-eu-west-1.amazonaws.com/landingi-editor-uploads/9edfaEKd/favicon.png" type="image/x-icon">
        <title>Landing Page Chatbot</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

        <!-- Chat CSS -->
        <link rel="stylesheet prefetch" href="theme/css/botui.min.css" />
        <link rel="stylesheet prefetch" href="theme/css/botui-theme-default.css" />
        <!-- Custom CSS -->
        <link href="theme/css/style.css" rel="stylesheet" />
        
        <script src="theme/js/jquery.min.js"></script>
    </head>
    <body>
        <nav class="white lighten-1" role="navigation">
            <div class="nav-wrapper container">
                <a id="logo-container" href="#" class="brand-logo">
                    <img src="https://images.assets-landingi.com/1ZA3c9jzy8jBcgeP/logo_org.png"/>
                </a>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a class="black-text" href="#">tel: +48 882 591 571</a>
                    </li>
                    <li>
                        <a class="waves-effect waves-light btn light-blue">Umów rozmowę</a>
                    </li>
                </ul>

                <ul id="nav-mobile" class="sidenav">
                    <li>
                        <a class="black-text" href="#">tel: +48 882 591 571</a>
                    </li>                    
                    <li>
                        <a class="waves-effect waves-light btn-large light-blue">Umów rozmowę</a>
                    </li>
                </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger">
                    <i class="material-icons black-text">menu</i>
                </a>
            </div>
        </nav>
        <div class="section no-pad-bot" id="index-banner">
            <div class="container">
                <br><br>
                <h2 class="header center white-text">
                    Stworzymy Landing Page<br />specjalnie dla Ciebie
                </h2>
                <div class="row center">
                    <h5 class="header col s12 light white-text">
                        w ramach platformy Landingi
                    </h5>
                </div>
                <div class="row center">
                    <a id="startConversation" href="#!" id="download-button" class="btn-large waves-effect waves-light white black-text">
                        Zaczynajmy!
                    </a>
                </div>
                <br><br>

            </div>
        </div>

        <input id="setDatepicker" type="text" class="datepicker" style="display: none !important">        
        <input id="setTimepicker" type="text" class="timepicker" style="display: none !important">

        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s1" style="position: relative">
                        <div id="avatar" style="position: absolute; right: -15px">
                            <div id="avatarImg" style="display: none"></div>
                        </div>
                    </div>
                    <div class="col s11">
                        <div class="botui-app-container" id="bot">
                            <bot-ui></bot-ui>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
       

        <!-- Modal Trigger -->
        <button style="display: none" id="modalForIdleUsers" data-target="modal1" class="btn modal-trigger">Modal</button>

        <!-- Modal Structure -->
        <div id="modal1" class="modal">
            <div class="modal-content pink darken-1">
                <h4 class="white-text center">
                    Odbierz Darmowy Landing Page
                </h4>
                <p class="white-text center">
                    w rocznym planie Create lub Automate!
                </p>
                <p>&nbsp;</p>
                <div class="center">
                    <a href="https://new.landingi.com/payments" target="_blank" class="waves-effect waves-light btn-large light-blue darken-1">
                        <i class="material-icons right">touch_app</i>
                        Skorzystaj z promocji
                    </a>
                    <br /><br />
                    <a class="modal-close waves-effect waves-light btn-small blue-grey lighten-5 black-text">
                        Nie, dziękuję
                    </a>
            </div>
        </div>

        <!--  Scripts-->
        <script src="js/materialize.js"></script>
        <script src="js/init.js"></script>

        <!-- Chat JS -->
        <script type="text/javascript">
            function avatarResize() {
                var clientHeight = document.getElementById("bot").clientHeight;
                document.getElementById("avatar").style.marginTop = clientHeight - 156 + "px";
            }
            $(document).ready(function () {
                $("body").bind("DOMSubtreeModified", function () {
                    window.scrollTo(0, document.body.scrollHeight);
                    avatarResize();
                });
                $(".modal").modal();
                $(".datepicker").datepicker({
                    defaultDate: new Date(),
                    setDefaultDate: false,
                    disableWeekends: true,
                    firstDay: 1,
                    i18n: {
                        cancel: "Zamknij",
                        done: "Gotowe",
                        months: [
                            "Styczeń",
                            "Luty",
                            "Marzec",
                            "Kwiecień",
                            "Maj",
                            "Czerwiec",
                            "Lipiec",
                            "Sierpień",
                            "Wrzesień",
                            "Październik",
                            "Listopad",
                            "Grudzień"
                        ],
                        monthsShort: [
                            "Sty",
                            "Lut",
                            "Mar",
                            "Kwi",
                            "Maj",
                            "Cze",
                            "Lip",
                            "Sie",
                            "Wrz",
                            "Paź",
                            "Lis",
                            "Gru"
                        ],
                        weekdays: [
                            "Niedziela",
                            "Poniedziałek",
                            "Wtorek",
                            "Środa",
                            "Czwartek",
                            "Piątek",
                            "Sobota"
                        ],
                        weekdaysShort: [
                            "Nie",
                            "Pon",
                            "Wto",
                            "Śro",
                            "Czw",
                            "Pią",
                            "Sob"
                        ],
                        weekdaysAbbrev: ["N","P","W","Ś","C","P","S"]
                    }
                });
                $(".timepicker").timepicker({
                    twelveHour: false,
                    i18n: {
                        cancel: "Zamknij",
                        done: "Gotowe"
                    }
                });
            });
            $(window).resize(function () {
                avatarResize();
            });
        </script>    
        <script src="theme/js/bioep.js"></script>
        <script src="theme/js/ea731dcb6f.js"></script>
        <script src="theme/js/es6-promise.min.js"></script>
        <script src="theme/js/vue.min.js"></script>
        <script src="theme/js/botui.min.js"></script>
        <script src="theme/js/screenplay/' . $templateId . '.js"></script>
        <script src="theme/js/index.js"></script>

    </body>
</html>
          ';

        $path = '/welcome.blade.php';
        if (!Storage::disk('view')->put($path, $template)) {
            return false;
        }
    }
    
    private function setPublish(int $templateId = NULL)
    {
        TemplatesRepository::publish($templateId);
    }
}
