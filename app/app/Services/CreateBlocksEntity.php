<?php
namespace App\Services;

use App\Repositories\BlocksEntity\Repository as BlocksEntityRepository;

class CreateBlocksEntity
{

    private $messages = NULL;
    private $buttons = NULL;
    private $switchToBlock = NULL;
    private $blockId = NULL;
    
    public function setMessages(array $messages = NULL)
    {
        $this->messages = (! empty($messages)) ? $messages : $this->messages;
    }
    
    public function setButtons(array $buttons = NULL)
    {
        $this->buttons = (! empty($buttons)) ? $buttons : $this->buttons;
    }
    
    public function setSwitchToBlock(array $switchToBlock = NULL)
    {
        $this->switchToBlock = (! empty($switchToBlock)) ? $switchToBlock : $this->switchToBlock;
    }
    
    public function setBlockId(int $blockId = NULL)
    {
        $this->blockId = (! empty($blockId)) ? $blockId : $this->blockId;
    }
    
    public function create()
    {
        BlocksEntityRepository::delete($this->blockId);

        if (!empty($this->messages)) {

            foreach ($this->messages as $messagesKey => $messagesItem) { 

                BlocksEntityRepository::create(
                    $this->blockId, 
                    $messagesKey, 
                    $messagesItem
                );
            }
        }

        if (!empty($this->buttons)) {

            foreach ($this->buttons as $buttonsKey => $buttonsItem) {

                BlocksEntityRepository::create(
                    $this->blockId, 
                    $buttonsKey, 
                    $buttonsItem
                );
            }
        }

        if (!empty($this->switchToBlock)) {

            foreach ($this->switchToBlock as $switchToBlockKey => $switchToBlockItem) {

                BlocksEntityRepository::create(
                    $this->blockId, 
                    $switchToBlockKey, 
                    $switchToBlockItem
                );
            }
        }
    }
}
