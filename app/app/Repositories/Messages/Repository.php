<?php
namespace App\Repositories\Messages;

use App\Messages as MessagesModel;
use App\Resources\Messages as MessagesResource;

class Repository extends MessagesResource
{
    public static $paginate = 20;

    /* @var data form model */
    public static $data;

    /**
     * Save new message
     * 
     * @param array $data
     * @return object
     */
    public static function create(array $data = [])
    {
        $data['content'] = MessagesResource::removeEmoji($data['content']);
        static::$data = MessagesModel::create($data);

        return static::$data;
    }

    /**
     * Count all leads
     * 
     * @return int
     */
    public static function countLeads()
    {
        static::$data = MessagesModel::select('leadId')
            ->groupBy('leadId')
            ->get()
            ->count();

        return static::$data;
    }

    /**
     * Count all conversations
     * 
     * @return int
     */
    public static function countConversations()
    {
        static::$data = MessagesModel::select('leadId')
            ->groupBy('leadId', 'conversationId')
            ->get()
            ->count();

        return static::$data;
    }

    /**
     * Count all conversations with answers
     * 
     * @return int
     */
    public static function countConversationsWithAnswers()
    {
        static::$data = MessagesModel::selectRaw('leadId, count(leadId) as count')
            ->where('count', '>', 4)
            ->groupBy('leadId', 'conversationId')
            ->get()
            ->count();

        return static::$data;
    }

    /**
     * Get all conversations with answers
     * 
     * @return object
     */
    public static function getConversationsWithAnswers(
        string $dateFrom = "", 
        string $dateTo = ""
    ) {
        static::$data = MessagesModel::selectRaw('leadId, conversationId')
            ->where('count', '>', 4)
            ->where('created_at', '>=', $dateFrom)
            ->where('created_at', '<=', $dateTo)
            ->groupBy('leadId', 'conversationId')
            ->orderBy('conversationId', 'desc')
            ->paginate(static::$paginate);

        return static::$data;
    }

    /**
     * Get all messages by conversation
     * 
     * @param int $leadId
     * @param int $conversationId
     * @return object
     */
    public static function getMessages(
        string $leadId = NULL, 
        string $conversationId = NULL
    ) {
        static::$data = MessagesModel::where('leadId', $leadId)
            ->where('conversationId', $conversationId)
            ->get();

        return static::$data;
    }
    
    /**
     * Get min created_at value
     * 
     * @return object
     */
    public static function getMinCreatedAt()
    {
        static::$data = MessagesModel::select('created_at')->orderBy('created_at', 'asc');
        $count = static::$data->get()->count();
        
        return MessagesResource::getFormatMinCreatedAt($count, static::$data);
    }
    
    /**
     * Get converstions on true
     * 
     * @return object
     */
    public static function getConversationsOnTrue()
    {
        static::$data = MessagesModel::select('created_at')
            ->where('content', 'LIKE', 'Tak')
            ->get()
            ->count();
        
        return static::$data;
    }
    
    /**
     * Get conversations on false
     * 
     * @return object
     */
    public static function getConversationsOnFalse()
    {
        static::$data = MessagesModel::select('created_at')
            ->where('content', 'LIKE', 'Nie')
            ->get()
            ->count();
        
        return static::$data;
    }
}
