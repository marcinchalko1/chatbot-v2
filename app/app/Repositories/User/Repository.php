<?php
namespace App\Repositories\User;

use App\User as UserModel;
use App\Resources\User as UserResource;

class Repository extends UserResource
{

    public static $data;

    public static function getByApiToken(string $token = '')
    {
        static::$data = UserModel::where('api_token', 'LIKE', $token)->firstOrFail()->getOriginal();

        return parent::getObject();
    }
}
