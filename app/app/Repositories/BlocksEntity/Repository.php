<?php
namespace App\Repositories\BlocksEntity;

use App\BlocksEntity as BlocksEntityModel;
use App\Resources\BlocksEntity as BlocksEntityResource;

class Repository extends BlocksEntityResource
{
    public static $paginate = 20;
    public static $dateFormat = "Y-m-d H:i:s";

    /* @var data form model */
    public static $data;

    /**
     * Save new block
     * 
     * @param array $data
     * @return object
     */
    public static function create(
        int $blockId = NULL, 
        int $sequence = 0, 
        array $data = []
    ) {
        if (!$sequence && $data['type'] == 1) {
            $data['parametrs']['delay'] = "0";
        }
        if (isset($data['parametrs']['delay']) && is_null($data['parametrs']['delay'])) {
            $data['parametrs']['delay'] = "0";
        }
        $data['block_id'] = $blockId;
        $data['created_at'] = date(static::$dateFormat);
        $data['updated_at'] = date(static::$dateFormat);
        $data['parametrs'] = json_encode($data['parametrs']);
        
        static::$data = BlocksEntityModel::create($data);

        return static::$data->id;
    }

    /**
     * Update block
     * 
     * @param int $templateId
     * @param string $name
     * @return object
     */
    public static function update(int $templateId = NULL, string $name = NULL)
    {        
        $dataToUpdate = array(
            'name' => $name, 
            'updated_at' => date(static::$dateFormat)
        );
        
        static::$data = BlocksEntityModel::where('template_id', $templateId)
            ->update($dataToUpdate);

        return $templateId;
    }
    
    /**
     * Get block by `block_entity_id`
     * 
     * @param int $blockEntityId
     * @return object
     */
    public static function getBlock(int $blockEntityId = NULL) 
    {
        static::$data = BlocksEntityModel::where(
            'block_entity_id', 
            $blockEntityId
        )->first();

        return static::$data;
    }
    
    /**
     * Get all blocks entity by `block_id`
     * 
     * @param int $blockId
     * @return object
     */
    public static function getBlocksEntityByBlockId(int $blockId = NULL) 
    {
        static::$data = BlocksEntityModel::selectRaw(
            'block_entity_id, type, parametrs, sequence')
            ->where('block_id', $blockId)            
            ->orderBy('sequence', 'asc')
            ->get();

        return static::$data;
    }
    
    public static function delete(int $blockId = NULL) 
    {
        static::$data = BlocksEntityModel::where('block_id', '=', $blockId)
            ->delete();

        return static::$data;
    }
}
