<?php
namespace App\Repositories\Templates;

use App\Templates as TemplatesModel;
use App\Resources\Templates as TemplatesResource;

class Repository extends TemplatesResource
{
    public static $paginate = 20;
    public static $dateFormat = "Y-m-d H:i:s";

    /* @var data form model */
    public static $data;

    /**
     * Save new template
     * 
     * @param array $data
     * @return object
     */
    public static function create(array $data = [])
    {
        $data['created_at'] = date(static::$dateFormat);
        $data['updated_at'] = date(static::$dateFormat);
        
        static::$data = TemplatesModel::create($data);

        return static::$data->id;
    }

    /**
     * Update template
     * 
     * @param int $templateId
     * @param string $name
     * @return object
     */
    public static function update(int $templateId = NULL, string $name = NULL)
    {        
        $dataToUpdate = array(
            'name' => $name, 
            'updated_at' => date(static::$dateFormat)
        );
        
        static::$data = TemplatesModel::where('template_id', $templateId)
            ->update($dataToUpdate);

        return $templateId;
    }
    
    /**
     * Get template by `template_id`
     * 
     * @param int $templateId
     * @return object
     */
    public static function getTemplate(int $templateId = NULL) 
    {
        static::$data = TemplatesModel::where('template_id', $templateId)->first();

        return static::$data;
    }
    
    /**
     * Get all templates
     * 
     * @return object
     */
    public static function getTemplates() 
    {
        static::$data = TemplatesModel::selectRaw('template_id, name, public')->get();

        return static::$data;
    }
    
    public static function delete(int $templateId = NULL) 
    {
        static::$data = TemplatesModel::where('template_id', '=', $templateId)
            ->delete();

        return static::$data;
    }
    
    public static function publish(int $templateId = NULL) 
    {
        $setUnpublish = array(
            'public' => 0
        );
        
        TemplatesModel::where('public', '<>', 0)->update($setUnpublish);
                
        $setPublish = array(
            'public' => 1
        );
        
        static::$data = TemplatesModel::where('template_id', $templateId)
            ->update($setPublish);
        

        return static::$data;
    }
        
    public static function isPublish(int $templateId = NULL) 
    {
        static::$data = TemplatesModel::selectRaw('public')
            ->where('template_id', $templateId)
            ->first();

        return static::$data->public;
    }
}
