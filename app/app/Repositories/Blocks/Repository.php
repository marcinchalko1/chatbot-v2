<?php
namespace App\Repositories\Blocks;

use App\Blocks as BlocksModel;
use App\BlocksEntity as BlocksEntityModel;
use App\Resources\Blocks as BlocksResource;
use App\Resources\BlocksEntity as BlocksEntityResource;

class Repository extends BlocksResource
{
    public static $paginate = 20;
    public static $dateFormat = "Y-m-d H:i:s";
    public static $defalutBlockName = 'Powitanie';

    /* @var data form model */
    public static $data;

    /**
     * Save new block
     * 
     * @param array $data
     * @return object
     */
    public static function create(array $data = [], int $templateId = NULL)
    {
        $data['template_id'] = $templateId;
        $data['created_at'] = date(static::$dateFormat);
        $data['updated_at'] = date(static::$dateFormat);
        
        static::$data = BlocksModel::create($data);

        return static::$data->id;
    }

    /**
     * Return default blocks
     * 
     * @param int $templateId
     * @return object
     */
    public static function getDefaultBlocks(int $templateId = NULL)
    {
        return parent::getDefaultBlocks($templateId);
    }

    /**
     * Save default blocks
     * 
     * @param int $templateId
     * @return object
     */
    public static function createDefault(int $templateId = NULL)
    {
        $blockData = [
            'template_id' => $templateId, 
            'name' => self::$defalutBlockName
        ];
        
        static::$data = self::create($blockData);
        
        $defaultBlocksEntity = BlocksEntityResource::getDefaultBlocksEntity(
            static::$data
        );
        
        if (is_array($defaultBlocksEntity) && !empty($defaultBlocksEntity)) {
            
            foreach ($defaultBlocksEntity as $defaultBlocksEntityItem) {
                
                BlocksEntityModel::create($defaultBlocksEntityItem);
                $boolean = true;
            }
        }

        return $boolean;
    }

    /**
     * Update block
     * 
     * @param int $blockId
     * @param string $name
     * @return object
     */
    public static function update(int $blockId = NULL, string $name = NULL)
    {        
        $dataToUpdate = array(
            'name' => $name, 
            'updated_at' => date(static::$dateFormat)
        );
        
        static::$data = BlocksModel::where('block_id', $blockId)
            ->update($dataToUpdate);

        return $blockId;
    }
    
    /**
     * Get block by `block_id`
     * 
     * @param int $blockId
     * @return object
     */
    public static function getBlock(int $blockId = NULL) 
    {
        static::$data = BlocksModel::where('block_id', $blockId)->first();

        return static::$data;
    }
    
    /**
     * Get all blocks by `template_id`
     * 
     * @param int $templateId
     * @return object
     */
    public static function getBlocksByTemplateId(int $templateId = NULL) 
    {
        static::$data = BlocksModel::selectRaw('block_id, name')
            ->where('template_id', $templateId)
            ->get();

        return static::$data;
    }
    
    /**
     * Get all blocks by `template_id`
     * 
     * @param int $templateId
     * @return object
     */
    public static function getFirstBlockByTemplateId(int $templateId = NULL) 
    {
        static::$data = BlocksModel::selectRaw('block_id, name')
            ->where('template_id', $templateId)
            ->first();

        return static::$data;
    }
    
    public static function delete(int $blockId = NULL) 
    {
        static::$data = BlocksModel::where('block_id', '=', $blockId)
            ->delete();

        return static::$data;
    }
    
    public static function getEnd()
    {
        static::$data = parent::getEnd();
    }
}
