<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = ['leadId', 'campaignId', 'conversationId', 'count', 'content'];
    protected $table = 'messages';
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

}
