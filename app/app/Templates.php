<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    protected $fillable = ['template_id', 'name', 'public', 'created_at', 'updated_at'];
    protected $table = 'templates';
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

}
