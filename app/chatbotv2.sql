-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: godformejk.nazwa.pl:3306
-- Czas generowania: 19 Lip 2018, 17:21
-- Wersja serwera: 10.1.26-MariaDB
-- Wersja PHP: 5.5.9-1ubuntu4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `godformejk`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blocks`
--

CREATE TABLE `blocks` (
  `block_id` int(11) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `blocks`
--

INSERT INTO `blocks` (`block_id`, `template_id`, `name`, `created_at`, `updated_at`) VALUES
(2, 7, 'Powitanie', '2018-07-13 20:54:30', '2018-07-13 20:54:30'),
(4, 7, 'default 23', '2018-07-14 13:57:22', '2018-07-14 14:55:18');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blocks_entity`
--

CREATE TABLE `blocks_entity` (
  `block_entity_id` int(11) NOT NULL,
  `block_id` int(11) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `sequence` tinyint(3) NOT NULL DEFAULT '1',
  `parametrs` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `blocks_entity`
--

INSERT INTO `blocks_entity` (`block_entity_id`, `block_id`, `type`, `sequence`, `parametrs`, `created_at`, `updated_at`) VALUES
(50, 2, 1, 1, '{\"content\":\"Witaj\",\"delay\":\"3000\",\"loading\":\"on\"}', '2018-07-17 21:27:49', '2018-07-17 21:27:49'),
(51, 2, 2, 2, '{\"action\":{\"text\":\"Witaj\",\"value\":null},\"delay\":\"0\"}', '2018-07-17 21:27:49', '2018-07-17 21:27:49'),
(52, 2, 2, 4, '{\"action\":{\"text\":\"Witaj dodane\",\"value\":\"default 23\"},\"delay\":\"0\"}', '2018-07-17 21:27:49', '2018-07-17 21:27:49'),
(53, 2, 1, 3, '{\"content\":\"Witaj dodane\",\"delay\":\"4000\",\"loading\":\"on\"}', '2018-07-17 21:27:49', '2018-07-17 21:27:49');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `leadId` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaignId` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `conversationId` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `messages`
--

INSERT INTO `messages` (`id`, `leadId`, `campaignId`, `conversationId`, `count`, `content`, `created_at`, `updated_at`) VALUES
(1, '10ed53eb-55a2-2eb2-d40b-2b92df12d1b8-1524700800102', '2', '1524700800105', '1', 'Witaj, skoro tu jesteś, miałbym do przekazania Ci prezent', '2018-07-18 07:51:03', '2018-07-18 07:51:03'),
(2, '10ed53eb-55a2-2eb2-d40b-2b92df12d1b8-1524700800102', '2', '1524700800105', '2', 'w postaci darmowego e-booka', '2018-07-18 07:51:04', '2018-07-18 07:51:04'),
(3, '10ed53eb-55a2-2eb2-d40b-2b92df12d1b8-1524700800102', '2', '1524700800105', '3', '[![gratis](https://www.pocztazdrowia.pl/resource/img/10klamstw.jpg)](http://landing.pocztazdrowia.pl/10klamstw/PZ2536EWW10)^', '2018-07-18 07:51:04', '2018-07-18 07:51:04'),
(4, '10ed53eb-55a2-2eb2-d40b-2b92df12d1b8-1524700800102', '2', '1524700800105', '4', 'Czy chcesz go otrzymać?', '2018-07-18 07:51:04', '2018-07-18 07:51:04'),
(5, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '1', 'Witaj! ', '2018-07-18 09:04:28', '2018-07-18 09:04:28'),
(6, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '2', '![gratis](images/hi.jpg)', '2018-07-18 09:04:30', '2018-07-18 09:04:30'),
(7, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '2', 'Cieszę się, że mogę z Tobą porozmawiać za pośrednictwem Bota. ', '2018-07-18 09:04:31', '2018-07-18 09:04:31'),
(8, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '2', 'Zachowuje się, jakby był mną, ale nie jest  żywą istotą', '2018-07-18 09:04:33', '2018-07-18 09:04:33'),
(9, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '2', 'Albo.. no cóż.. aktualnie robi za mnie robotę.  ', '2018-07-18 09:04:36', '2018-07-18 09:04:36'),
(10, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '10000', 'Cześć bocie Marcinie!', '2018-07-18 09:04:48', '2018-07-18 09:04:48'),
(11, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', '![gratis](images/ok.jpg)', '2018-07-18 09:04:50', '2018-07-18 09:04:50'),
(12, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '3', 'Od niedawna zacząłem się interesować chatbotami.', '2018-07-18 09:04:52', '2018-07-18 09:04:52'),
(13, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '3', 'Czy chcesz dowiedzieć się co to jest?', '2018-07-18 09:04:54', '2018-07-18 09:04:54'),
(14, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '5', 'Niekoniecznie, wiem co to jest chatbot.', '2018-07-18 09:05:00', '2018-07-18 09:05:00'),
(15, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '6', 'W tym momencie będę szczery..', '2018-07-18 09:05:02', '2018-07-18 09:05:02'),
(16, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'Po prostu się nimi zauroczyłem. ', '2018-07-18 09:05:04', '2018-07-18 09:05:04'),
(17, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', '![gratis](https://media.giphy.com/media/5jSFQ2xmJKBaw/giphy.gif)', '2018-07-18 09:05:06', '2018-07-18 09:05:06'),
(18, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'I postanowiłem się z chatbotami zaprzyjaźnić.', '2018-07-18 09:05:08', '2018-07-18 09:05:08'),
(19, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '8', 'Czy mam kontynuować? ', '2018-07-18 09:05:11', '2018-07-18 09:05:11'),
(20, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '10000', 'LOL!  Pewnie, kontynuuj.', '2018-07-18 09:05:14', '2018-07-18 09:05:14'),
(21, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', '![gratis](https://thumbs.gfycat.com/MixedFakeIchneumonfly-small.gif)', '2018-07-18 09:05:16', '2018-07-18 09:05:16'),
(22, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '6', 'Zamierzam wdrażać chatboty  na stronach internetowych.', '2018-07-18 09:05:18', '2018-07-18 09:05:18'),
(23, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'Na początek dodawać do nich boty konwersacyjne, które przekażą użytkownikowi najważniejsze informacje o danej witrynie internetowej.', '2018-07-18 09:05:20', '2018-07-18 09:05:20'),
(24, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'Automatycznie wchodząc z nim w interakcję.', '2018-07-18 09:05:22', '2018-07-18 09:05:22'),
(25, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'Odważniejszą wersją jest zastąpienie strony internetowej chatbotem. ', '2018-07-18 09:05:24', '2018-07-18 09:05:24'),
(26, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '8', '![gratis](https://media.giphy.com/media/W0gsDeNrwCYKY/giphy.gif)', '2018-07-18 09:05:26', '2018-07-18 09:05:26'),
(27, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'Tak, jak w moim przypadku. ', '2018-07-18 09:05:28', '2018-07-18 09:05:28'),
(28, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '5', 'Ok, już rozumiem.', '2018-07-18 09:05:33', '2018-07-18 09:05:33'),
(29, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '6', 'Powiedz mi, jak oceniasz rozmowę z.. Botem ', '2018-07-18 09:05:35', '2018-07-18 09:05:35'),
(30, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'tylko proszę, szczerze.  ️', '2018-07-18 09:05:37', '2018-07-18 09:05:37'),
(31, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '10000', 'Nie zainteresował mnie wystarczająco ', '2018-07-18 09:05:41', '2018-07-18 09:05:41'),
(32, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '6', 'Jeśli wystarczająco zainteresowałem Cię zastosowaniem chatbota', '2018-07-18 09:05:43', '2018-07-18 09:05:43'),
(33, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'na stronach internetowych', '2018-07-18 09:05:45', '2018-07-18 09:05:45'),
(34, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'chętnie porozmawiam z Tobą na ten temat', '2018-07-18 09:05:47', '2018-07-18 09:05:47'),
(35, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '8', 'telefonicznie lub e-mailowo, a następnie może przy ️.', '2018-07-18 09:05:49', '2018-07-18 09:05:49'),
(36, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'Czy przyjmujesz ode mnie zaproszenie? ', '2018-07-18 09:05:51', '2018-07-18 09:05:51'),
(37, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '5', 'Tak, chętnie!', '2018-07-18 09:05:59', '2018-07-18 09:05:59'),
(38, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '6', 'Czuję się zaszczycony. ', '2018-07-18 09:06:01', '2018-07-18 09:06:01'),
(39, 'a09604d7-72bc-7136-c93e-cbf5c330ecd5-1531904643680', '2', '1531904643680', '7', 'Proszę, wybierz kanał komunikacji, który najbardziej Ci odpowiada.', '2018-07-18 09:06:03', '2018-07-18 09:06:03'),
(40, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '1', 'Witaj! ', '2018-07-18 10:47:11', '2018-07-18 10:47:11'),
(41, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '2', '![gratis](images/hi.jpg)', '2018-07-18 10:47:12', '2018-07-18 10:47:12'),
(42, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '2', 'Cieszę się, że mogę z Tobą porozmawiać za pośrednictwem Bota. ', '2018-07-18 10:47:14', '2018-07-18 10:47:14'),
(43, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '2', 'Zachowuje się, jakby był mną, ale nie jest  żywą istotą', '2018-07-18 10:47:17', '2018-07-18 10:47:17'),
(44, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '2', 'Albo.. no cóż.. aktualnie robi za mnie robotę.  ', '2018-07-18 10:47:19', '2018-07-18 10:47:19'),
(45, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '10000', 'Cześć bocie Marcinie!', '2018-07-18 10:47:28', '2018-07-18 10:47:28'),
(46, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', '![gratis](images/ok.jpg)', '2018-07-18 10:47:31', '2018-07-18 10:47:31'),
(47, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '3', 'Od niedawna zacząłem się interesować chatbotami.', '2018-07-18 10:47:33', '2018-07-18 10:47:33'),
(48, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '3', 'Czy chcesz dowiedzieć się co to jest?', '2018-07-18 10:47:35', '2018-07-18 10:47:35'),
(49, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '5', 'Niekoniecznie, wiem co to jest chatbot.', '2018-07-18 10:47:45', '2018-07-18 10:47:45'),
(50, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '6', 'W tym momencie będę szczery..', '2018-07-18 10:47:47', '2018-07-18 10:47:47'),
(51, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', 'Po prostu się nimi zauroczyłem. ', '2018-07-18 10:47:49', '2018-07-18 10:47:49'),
(52, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', '![gratis](https://media.giphy.com/media/5jSFQ2xmJKBaw/giphy.gif)', '2018-07-18 10:47:52', '2018-07-18 10:47:52'),
(53, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', 'I postanowiłem się z chatbotami zaprzyjaźnić.', '2018-07-18 10:47:53', '2018-07-18 10:47:53'),
(54, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '8', 'Czy mam kontynuować? ', '2018-07-18 10:47:55', '2018-07-18 10:47:55'),
(55, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '10000', 'LOL!  Pewnie, kontynuuj.', '2018-07-18 10:47:58', '2018-07-18 10:47:58'),
(56, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', '![gratis](https://thumbs.gfycat.com/MixedFakeIchneumonfly-small.gif)', '2018-07-18 10:48:00', '2018-07-18 10:48:00'),
(57, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '6', 'Zamierzam wdrażać chatboty  na stronach internetowych.', '2018-07-18 10:48:02', '2018-07-18 10:48:02'),
(58, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', 'Na początek dodawać do nich boty konwersacyjne, które przekażą użytkownikowi najważniejsze informacje o danej witrynie internetowej.', '2018-07-18 10:48:04', '2018-07-18 10:48:04'),
(59, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', 'Automatycznie wchodząc z nim w interakcję.', '2018-07-18 10:48:06', '2018-07-18 10:48:06'),
(60, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', 'Odważniejszą wersją jest zastąpienie strony internetowej chatbotem. ', '2018-07-18 10:48:08', '2018-07-18 10:48:08'),
(61, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '8', '![gratis](https://media.giphy.com/media/W0gsDeNrwCYKY/giphy.gif)', '2018-07-18 10:48:10', '2018-07-18 10:48:10'),
(62, 'fb7c2f23-eebf-72c6-513c-745f7ca46cfa-1531910816672', '2', '1531910816672', '7', 'Tak, jak w moim przypadku. ', '2018-07-18 10:48:13', '2018-07-18 10:48:13');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_14_063820_create_products_table', 2),
(4, '2018_04_14_153054_adds_api_token_to_users_table', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `templates`
--

CREATE TABLE `templates` (
  `template_id` int(11) NOT NULL,
  `name` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `templates`
--

INSERT INTO `templates` (`template_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Demo 11', '2018-07-11 21:08:52', '2018-07-12 19:36:53'),
(2, 'Demo 2', '2018-07-11 21:09:36', '2018-07-11 22:36:17'),
(3, 'Demo 1', '2018-07-11 22:11:08', '2018-07-11 22:11:08'),
(7, 'default', '2018-07-13 20:54:30', '2018-07-13 20:54:30');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `api_token`) VALUES
(2, 'Marcin', 'marcin.chalko@gmail.com', '$2y$10$QwntXJRkc5aHPoK2eZi9xuzxWvTX5XwPNhhTN17oWs33Yl2bnXYoS', '6oGTWqdDflJ6CN4pS6mkjC28sdSmPL1ZBnhcObz21ktix2fVNYyI49dIKP1S', '2018-04-14 14:21:53', '2018-04-14 15:33:17', 'WLonKahKVz5GbmSfgnniblkqRGYrYiwnMa8bHMoBPA60aYWGN2WI9F6fO61B'),
(3, 'Kuba', 'robert@oltarzewski.pl', '$2y$10$w.NAaydph/f.VO8azye9z.YDOF3OuyvYU9WXEpanLJ64tTtmFioiq', 'KCN4JGIe9AKsdEhOkPAYTQmJSJI2p4UdbPcviCr2bK5W5PahpNA1FGtds3QC', '2018-04-18 19:13:59', '2018-04-18 19:13:59', '38EyvNYc6XejjFWy8mN8xRZTpiCr1knhRgDzi69T7r86Qhw7p4SuJEM9o7xU'),
(5, 'test', 'khrusza@nwi.com.pl', '$2y$10$5xW95KSmBgLJUEIcdgLeYe7.AtETPxJTEr8KlfKAjJ00yIzsDP7k2', NULL, '2018-04-20 05:59:30', '2018-04-20 05:59:30', '37ZrVtG5MJPyt8LGlJjleI1mK56SepQgnnWFfeWYcuRHGO4ARAWEBp7Qnw2K');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`block_id`),
  ADD KEY `fk_template_id` (`template_id`);

--
-- Indeksy dla tabeli `blocks_entity`
--
ALTER TABLE `blocks_entity`
  ADD PRIMARY KEY (`block_entity_id`),
  ADD KEY `fk_block_id` (`block_id`);

--
-- Indeksy dla tabeli `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeksy dla tabeli `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`template_id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `blocks`
--
ALTER TABLE `blocks`
  MODIFY `block_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `blocks_entity`
--
ALTER TABLE `blocks_entity`
  MODIFY `block_entity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT dla tabeli `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT dla tabeli `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `templates`
--
ALTER TABLE `templates`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `blocks`
--
ALTER TABLE `blocks`
  ADD CONSTRAINT `fk_template_id` FOREIGN KEY (`template_id`) REFERENCES `templates` (`template_id`);

--
-- Ograniczenia dla tabeli `blocks_entity`
--
ALTER TABLE `blocks_entity`
  ADD CONSTRAINT `fk_block_id` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`block_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
