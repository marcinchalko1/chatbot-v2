<?php
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::post('register', 'Auth\RegisterController@register');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/templates/create', 'TemplatesController@create')->name('templates');
Route::post('/templates/store', 'TemplatesController@store')->name('templates');
Route::post('/templates/update/{templateId}', 'TemplatesController@update')->name('templates');
Route::get('/templates/form/{templateId}/{blockId?}', 'TemplatesController@form')->name('templates');
Route::get('/templates/delete/{templateId}', 'TemplatesController@delete')->name('templates');

Route::post('/blocks/store/{templateId}', 'BlocksController@store')->name('blocks');
Route::post('/blocks/update/{templateId}/{blockId}', 'BlocksController@update')->name('blocks');
Route::get('/blocks/delete/{templateId}/{blockId}', 'BlocksController@delete')->name('blocks');
Route::get('/blocks/defaults/{templateId}/{defaultBlockId}', 'BlocksController@defaults')->name('blocks');

Route::post('/blocksEntity/store/{templateId}/{blockId}', 'BlocksEntityController@store')->name('blocksEntity');

Route::get('/test/{templateId}', 'TestChatbotController@test')->name('test');

Route::get('/public/{templateId}', 'PublicChatbotController@production')->name('public');

Route::get('/chats', 'ChatsController@index')->name('chats');
Route::post('/chats', 'ChatsController@index')->name('chats');
Route::get(
    '/chats/messages/leadId/{leadId}/conversationId/{conversationId}', 
    'ChatsController@messages')
    ->name('messages');

Route::prefix('api')->group(function () {
    Route::namespace('Api')->group(function () {
        // production
        Route::get('messages/create', 'MessagesController@create');
        // development
        Route::get('test', 'TestController@get');
        Route::get('createMessage', 'TestController@createMessage');
    });
});

