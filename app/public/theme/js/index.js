//const CLASS_CIRCLE = '.circle';
//const CLASS_ICON = '.icon-elements';
//const CLASS_MODAL = '.modal-wrapper';
//const CLASS_ICON_ACTIVE = 'js-icon-active';
//const CLASS_MODAL_ACTIVE = 'js-modal-active';
//
//const elementCircle = document.querySelector(CLASS_CIRCLE);
//const elementIcon = document.querySelector(CLASS_ICON);
//const elementModal = document.querySelector(CLASS_MODAL);
//const elementInput = document.querySelector('#myInput');

//const triggerAnimation = () => {
//    const isActive = elementIcon.classList.contains(CLASS_ICON_ACTIVE);
//    console.log(isActive);
//    isActive ? (
//            elementIcon.classList.remove(CLASS_ICON_ACTIVE),
//            elementModal.classList.remove(CLASS_MODAL_ACTIVE)
//            ) : (
//            elementIcon.classList.add(CLASS_ICON_ACTIVE),
//            elementModal.classList.add(CLASS_MODAL_ACTIVE)
//            );
//};

//elementCircle.addEventListener('click', () => triggerAnimation());

function prefix() {
    return 'd3aacd5490d6677032800f3f17d54ad65f54e7bf' + '-';
}

function getCookie(cname) {
    var name = prefix() + cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var displayCounter = true;
var displaySound = false;
var displayExitPopup = true;
var displayPopupForIdleUsers = true;

$('.botui-container').on("DOMSubtreeModified", function () {
//    $('#bot').scrollTop($('#bot').scrollHeight);
//    window.scrollTo(0,document.querySelector(".popup-messages").scrollHeight);

//    $(".popup-messages").mCustomScrollbar("scrollTo","bottom");

    $('.messages-counter').html($('.botui-message').length);

    if (displayCounter) {
        $('.messages-counter').show();
        $('.messages-counter').html($('.botui-message').length);
    }

    if (displaySound && getCookie('leadId') == '') {
        $("#mysoundclip")[0].play();
    }
});

if (displayExitPopup) {
    bioEp.init({
        html: '<div class="container"><div class="row"><h4 class="white-text center">Odbierz Darmowy Landing Page</h4><p class="white-text center">w rocznym planie Create lub Automate!</p><p>&nbsp;</p><div class="center"><a href="https://new.landingi.com/payments" target="_blank" class="waves-effect waves-light btn-large light-blue darken-1"><i class="material-icons right">touch_app</i>Skorzystaj z promocji</a><br /><br /><a id="bio_ep_close" class="waves-effect waves-light btn-small blue-grey lighten-5 black-text">Nie, dziękuję</a></div></div></div>',
        css: '#content {font-family: "Titillium Web", sans-serif; font-size: 14px;}',
        cookieExp: 0
    });
}


if (displayPopupForIdleUsers) {   
    
    window.timerStatus = true;
    
    function startTimer() {
        if (window.timerStatus) {
            window.timer = setTimeout(function () {
                $('#modalForIdleUsers').click();               
                window.timerStatus = false;
            }, 5000);
        }
    };

    $(document).on('scroll mousemove', function () { // 'keydown' as well?
        clearTimeout(window.timer);
        startTimer();
    });
    
    startTimer(); // start it now
}