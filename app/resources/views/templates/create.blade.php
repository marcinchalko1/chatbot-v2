@extends('layouts.app')

@section('content')
        
<form class="mdform" method="post" action="{{ URL::to('/templates/store') }}">
    {{ csrf_field() }}
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        @if ($errors->has('name'))
                            <div class="alert alert-bold alert-danger" role="alert">
                                <strong>{{ $validation['name'][$errors->first('name')] }}</strong>
                            </div>
                        @endif
                        <div class="form-group md-form-group">
                            <label for="form1-email">Nazwa szablonu</label>
                            <input class="form-control" id="form1-email" type="text" name="name">
                        </div>
                    </div>
                    <!-- /.panel-body -->                    
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-info">Zapisz</button>
                    </div>
                </div>
                <!-- /.panel-primary panel -->
            </div>
        </div>
    </div>
</form>
@endsection
