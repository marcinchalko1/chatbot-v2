@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h2>
                        {{ $templateName }}
                        <button href="#" class="btn btn-xs btn-link pull-right" data-toggle="modal" data-target="#updateTemplateName">
                            <div class="fa fa-edit"></div>
                            Zmień nazwę
                        </button>
                        <a target="_blank" href="{{ URL::to('/test/'.$templateId) }}" class="btn btn-xs btn-link pull-right">
                            <div class="fa fa-edit"></div>
                            Podgląd
                        </a>
                    </h2>
                </div>
                <div class="modal fade" id="updateTemplateName" tabindex="-1" role="dialog" aria-labelledby="updateTemplateNameLabel" style="display: none;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="updateTemplateName">Zmień nazwę szablonu</h4>
                            </div>

                            <form class="mdform" method="post" action="{{ URL::to('/templates/update/'.$templateId) }}">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    @if ($errors->has('name'))
                                    <div class="alert alert-bold alert-danger" role="alert">
                                        <strong>{{ $validation['name'][$errors->first('name')] }}</strong>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="form1-email">Nazwa szablonu</label>
                                        <input class="form-control" id="form1-email" type="text" name="name" value="{{ $templateName }}">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                                    <button type="submit" class="btn btn-primary pull-right">Aktualizuj</button>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.panel-primary panel -->
        </div>

        <div class="col-md-5">
            <div class="panel">
                <div class="panel-heading">
                    <h2>Bloki</h2>
                </div>
                <div class="panel-body">
                    @if (!empty($blocks))
                        @foreach ($blocks as $blocksKey => $blocksItem)
                        <a href="{{ URL::to('/templates/form/'.$templateId.'/'.$blocksItem->block_id) }}" class="btn {{ ($blocksItem->block_id == $blockId) ? 'btn-primary' : 'btn-default' }}">
                            {{ $blocksItem->name }}
                        </a>
                        @endforeach
                    @endif
                </div>                
                <div class="panel-footer">
                    <a href="#" class="btn btn-danger"  data-toggle="modal" data-target="#addBlock">
                        <i class="fa fa-plus"></i>
                        Dodaj blok
                    </a>                    
                </div>
                <div class="modal fade" id="addBlock" tabindex="-1" role="dialog" aria-labelledby="addBlockLabel" style="display: none;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="addBlock">Dodaj blok</h4>
                            </div>
                            <form class="mdform" method="post" action="{{ URL::to('/blocks/store/'.$templateId) }}">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    @if ($errors->has('name'))
                                    <div class="alert alert-bold alert-danger" role="alert">
                                        <strong>{{ $validation['name'][$errors->first('name')] }}</strong>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="form1-email">Nazwa bloku</label>
                                        <input class="form-control" id="form1-email" type="text" name="name" />
                                    </div>                                    
                                    <a href="{{ URL::to('blocks/defaults/'.$templateId.'/1') }}" class="btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                        end
                                    </a>                                     
                                    <a href="{{ URL::to('blocks/defaults/'.$templateId.'/2') }}" class="btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                        byebye
                                    </a> 
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                                    <button type="submit" class="btn btn-primary pull-right">Stwórz</button>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>            

        <div class="col-md-7">
            @if (!empty($blockId))
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2>Blok: "{{ $blockName }}"                    
                        <button href="#" class="btn btn-xs btn-link pull-right" data-toggle="modal" data-target="#updateBlockName">
                            <div class="fa fa-edit"></div>
                            Zmień nazwę
                        </button>
                    </h2>
                    <div class="modal fade" id="updateBlockName" tabindex="-1" role="dialog" aria-labelledby="updateBlockNameLabel" style="display: none;">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="updateBlockName">Zmień nazwę bloku</h4>
                                </div>
                                <form class="mdform" method="post" action="{{ URL::to('/blocks/update/'.$templateId.'/'.$blockId) }}">
                                    {{ csrf_field() }}
                                    <div class="modal-body">
                                        @if ($errors->has('name'))
                                        <div class="alert alert-bold alert-danger" role="alert">
                                            <strong>{{ $validation['name'][$errors->first('name')] }}</strong>
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <label for="form1-email">Nazwa bloku</label>
                                            <input class="form-control" id="form1-email" type="text" name="name" value="{{ $blockName }}"/>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                                        <button type="submit" class="btn btn-primary pull-right">Stwórz</button>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-highlight-action">
                    <a href="{{ URL::to('blocks/delete/'.$templateId.'/'.$blockId) }}">
                        <button class="btn btn-round btn-danger btn-lg">
                            <i class="fa fa-trash"></i>
                        </button>
                    </a>
                </div>
                <form class="mdform" method="post" action="{{ URL::to('/blocksEntity/store/'.$templateId.'/'.$blockId) }}">
                    <div class="panel-body" id="block">
                        {{ csrf_field() }}
                        @if (!empty($blocksEntity))                            
                            <div class="panel">
                                <div class="panel-heading no-padding panel-item-raised">
                                    <h3>Zawartość</h3>
                                </div>
<!--                                    <div class="panel-highlight-action">
                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>-->
                                <div class="panel-body" id="messagesContainer">
                                    @foreach ($blocksEntity as $blocksEntityKey => $blocksEntityItem)
                                        @php
                                            $blockParametrs = json_decode($blocksEntityItem->parametrs);
                                        @endphp
                                        @if ($blocksEntityItem->type == 1)
                                            <div class="panel item">
                                                <div class="panel-heading no-padding panel-item-raised"></div>
                                                <div class="panel-highlight-action">
                                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                                <div class="panel-body">    
                                                    <input name="blocks[{{ $blocksEntityKey }}][1][type]" class="form-control" id="form2-email" type="hidden" value="1">
                                                    <div class="form-group ">
                                                        <label for="form2-email">Kolejność</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][1][sequence]" class="form-control" id="form2-email" type="text" value="{{ $blocksEntityItem->sequence }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Treść wiadomości</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][1][parametrs][content]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->content }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Opóźnienie w sekundach</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][1][parametrs][delay]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->delay }}">
                                                    </div>
                                                    <div class="form-group checkbox">
                                                        <label  class="">
                                                            <input name="blocks[{{ $blocksEntityKey }}][1][parametrs][loading]" type="checkbox" class="bootstrap-switch" {{ ($blockParametrs->loading) ? 'checked' : NULL }}> 
                                                            <span class="label-text">Pokaż "Pisze.."</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                        @endif
                                        @if ($blocksEntityItem->type == 4)
                                            <div class="panel item">
                                                <div class="panel-heading no-padding panel-item-raised">
                                                    <h3>Input</h3>
                                                </div>
                                                <div class="panel-highlight-action">
                                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                                <div class="panel-body">    
                                                    <input name="blocks[{{ $blocksEntityKey }}][4][type]" class="form-control" id="form2-email" type="hidden" value="4">
                                                    <div class="form-group ">
                                                        <label for="form2-email">Kolejność</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][4][sequence]" class="form-control" id="form2-email" type="text" value="{{ $blocksEntityItem->sequence }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Subtype</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][4][parametrs][action][sub_type]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->action->sub_type }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Placeholder</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][4][parametrs][action][placeholder]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->action->placeholder }}">
                                                    </div>
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                        @endif
                                        @if ($blocksEntityItem->type == 5)
                                            <div class="panel item">
                                                <div class="panel-heading no-padding panel-item-raised">
                                                    <h3>Link</h3>
                                                </div>
                                                <div class="panel-highlight-action">
                                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                                <div class="panel-body">    
                                                    <input name="blocks[{{ $blocksEntityKey }}][5][type]" class="form-control" id="form2-email" type="hidden" value="5">
                                                    <div class="form-group ">
                                                        <label for="form2-email">Kolejność</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][5][sequence]" class="form-control" id="form2-email" type="text" value="{{ $blocksEntityItem->sequence }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Href</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][5][parametrs][href]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->href }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Text</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][5][parametrs][text]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->text }}">
                                                    </div>
                                                    <div class="form-group checkbox">
                                                        <label  class="">
                                                            <input name="blocks[{{ $blocksEntityKey }}][5][parametrs][blank]" type="checkbox" class="bootstrap-switch" {{ ($blockParametrs->blank) ? 'checked' : NULL }}> 
                                                            <span class="label-text">Blank?</span>
                                                        </label>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Opóźnienie w sekundach</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][5][parametrs][delay]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->delay }}">
                                                    </div>
                                                    <div class="form-group checkbox">
                                                        <label  class="">
                                                            <input name="blocks[{{ $blocksEntityKey }}][5][parametrs][loading]" type="checkbox" class="bootstrap-switch" {{ ($blockParametrs->loading) ? 'checked' : NULL }}> 
                                                            <span class="label-text">Pokaż "Pisze.."</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                        @endif
                                        @if ($blocksEntityItem->type == 6)
                                            <div class="panel item">
                                                <div class="panel-heading no-padding panel-item-raised">
                                                    <h3>Image</h3>
                                                </div>
                                                <div class="panel-highlight-action">
                                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                                <div class="panel-body">    
                                                    <input name="blocks[{{ $blocksEntityKey }}][6][type]" class="form-control" id="form2-email" type="hidden" value="6">
                                                    <div class="form-group ">
                                                        <label for="form2-email">Kolejność</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][6][sequence]" class="form-control" id="form2-email" type="text" value="{{ $blocksEntityItem->sequence }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Alt</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][6][parametrs][alt]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->alt }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Src</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][6][parametrs][src]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->src }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Opóźnienie w sekundach</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][6][parametrs][delay]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->delay }}">
                                                    </div>
                                                    <div class="form-group checkbox">
                                                        <label  class="">
                                                            <input name="blocks[{{ $blocksEntityKey }}][6][parametrs][loading]" type="checkbox" class="bootstrap-switch" {{ ($blockParametrs->loading) ? 'checked' : NULL }}> 
                                                            <span class="label-text">Pokaż "Pisze.."</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                        @endif
                                    @endforeach 
                                </div>        
                            </div>       
                            <div class="panel">
                                <div class="panel-heading no-padding panel-item-raised">
                                    <h3>Przyciski</h3>
                                </div>
<!--                                    <div class="panel-highlight-action">
                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>-->
                                <div class="panel-body" id="buttonsContainer"> 
                                    @foreach ($blocksEntity as $blocksEntityKey => $blocksEntityItem)
                                        @php
                                            $blockParametrs = json_decode($blocksEntityItem->parametrs);
                                        @endphp
                                        @if ($blocksEntityItem->type == 2)
                                            <div class="panel item">
                                                <div class="panel-heading no-padding panel-item-raised"></div>
                                                <div class="panel-highlight-action">
                                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                                <div class="panel-body">
                                                    <input name="blocks[{{ $blocksEntityKey }}][2][type]" class="form-control" id="form2-email" type="hidden" value="2">
                                                    <div class="form-group ">
                                                        <label for="form2-email">Kolejność</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][2][sequence]" class="form-control" id="form2-email" type="text" value="{{ $blocksEntityItem->sequence }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Nazwa przycisku</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][2][parametrs][action][text]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->action->text }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Nazwa bloku</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][2][parametrs][action][value]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->action->value }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Opóźnienie w sekundach</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][2][parametrs][delay]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->delay }}">
                                                    </div>
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                        @endif 
                                    @endforeach  
                                </div>   
                            </div>        
                            <div class="panel">
                                <div class="panel-heading no-padding panel-item-raised">
                                    <h3>Przejścia do bloku</h3>
                                </div>
<!--                                    <div class="panel-highlight-action">
                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>-->
                                <div class="panel-body" id="switchToBlockContainer"> 
                                    @foreach ($blocksEntity as $blocksEntityKey => $blocksEntityItem)
                                        @php
                                            $blockParametrs = json_decode($blocksEntityItem->parametrs);
                                        @endphp
                                        @if ($blocksEntityItem->type == 3)
                                            <div class="panel item">
                                                <div class="panel-heading no-padding panel-item-raised"></div>
                                                <div class="panel-highlight-action">
                                                    <button class="btn btn-round btn-default btn-lg delete-block-entity">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                                <div class="panel-body">
                                                    <input name="blocks[{{ $blocksEntityKey }}][3][type]" class="form-control" id="form2-email" type="hidden" value="3">
                                                    <div class="form-group ">
                                                        <label for="form2-email">Kolejność</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][3][sequence]" class="form-control" id="form2-email" type="text" value="{{ $blocksEntityItem->sequence }}">
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="form2-email">Nazwa bloku</label>
                                                        <input name="blocks[{{ $blocksEntityKey }}][3][parametrs][action][value]" class="form-control" id="form2-email" type="text" value="{{ $blockParametrs->action->value }}">
                                                    </div>
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                        @endif 
                                    @endforeach  
                                </div>   
                            </div>  
                        @endif
                    </div>
                <!-- /.panel-body -->
                <div class="panel-footer">                    

                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 0">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-plus"></i>
                                    Dodaj element
                                </button>
                            </a>
                            <ul class="dropdown-menu notification-dropdown list-group list-group-linked list-group-wide ">
                                <li class="list-group-item list-group-footer">
                                    <a href="#!" class="link-wrapper add-text">
                                        <div class="media">
                                            <div class="media-full">
                                                <h6 class="nowrap align-center bold text-muted" style="padding:0;margin:0; font-weight:400">
                                                    POLE TEKSTOWE
                                                </h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item list-group-footer">
                                    <a href="#!" class="link-wrapper add-button">
                                        <div class="media">
                                            <div class="media-full">
                                                <h6 class="nowrap align-center bold text-muted" style="padding:0;margin:0; font-weight:400">
                                                    PRZYCISK
                                                </h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item list-group-footer">
                                    <a href="#!" class="link-wrapper add-input">
                                        <div class="media">
                                            <div class="media-full">
                                                <h6 class="nowrap align-center bold text-muted" style="padding:0;margin:0; font-weight:400">
                                                    POLE INPUT
                                                </h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item list-group-footer">
                                    <a href="#!" class="link-wrapper add-link">
                                        <div class="media">
                                            <div class="media-full">
                                                <h6 class="nowrap align-center bold text-muted" style="padding:0;margin:0; font-weight:400">
                                                    POLE LINK
                                                </h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item list-group-footer">
                                    <a href="#!" class="link-wrapper add-image">
                                        <div class="media">
                                            <div class="media-full">
                                                <h6 class="nowrap align-center bold text-muted" style="padding:0;margin:0; font-weight:400">
                                                    POLE IMAGE
                                                </h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item list-group-footer">
                                    <a href="#!" class="link-wrapper add-switchToBlock">
                                        <div class="media">
                                            <div class="media-full">
                                                <h6 class="nowrap align-center bold text-muted" style="padding:0;margin:0; font-weight:400">
                                                    PRZEJDŹ DO BLOKU
                                                </h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <button type="submit" class="btn btn-large btn-info pull-right">
                        <div class="fa fa-save"></div>
                        Zapisz
                    </button>
                    <div class="clearfix"></div>
                </div>
                </form>
            </div>
            @endif
        </div>
    </div>
</div>
<script type="text/javascript">
$(function () {
    $('.add-text').click(function () {
        var numItems = $('#messagesContainer .item').length+1;
        var template = '<div class="panel item"><div class="panel-heading no-padding panel-item-raised"><h3>Pole tekstowe</h3></div><div class="panel-highlight-action"><button class="btn btn-round btn-default btn-lg delete-block-entity"><i class="fa fa-trash"></i></button></div><div class="panel-body"><input name="blocks[' + numItems + '][1][type]" class="form-control" id="form2-email" type="hidden" value="1"><div class="form-group "><label for="form2-email">Kolejność</label><input name="blocks[' + numItems + '][1][sequence]" class="form-control" id="form2-email" type="text" value="' + numItems + '"></div><div class="form-group "><label for="form2-email">Treść wiadomości</label><input name="blocks[' + numItems + '][1][parametrs][content]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group "><label for="form2-email">Opóźnienie w sekundach</label><input name="blocks[' + numItems + '][1][parametrs][delay]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group checkbox"><label  class=""><input name="blocks[' + numItems + '][1][parametrs][loading]" type="checkbox" class="bootstrap-switch" checked><span class="label-text">Pokaż "Pisze.."</span></label></div></div><!-- /.panel-body --></div>';
        $('#messagesContainer').append(template);
    });
    $('.add-button').click(function () {
        var numItems = $('#buttonsContainer .item').length+1;
        var template = '<div class="panel item"><div class="panel-heading no-padding panel-item-raised"><h3>Przycisk</h3></div><div class="panel-highlight-action"><button class="btn btn-round btn-default btn-lg delete-block-entity"><i class="fa fa-trash"></i></button></div><div class="panel-body"><input name="blocks[' + numItems + '][2][type]" class="form-control" id="form2-email" type="hidden" value="2"><div class="form-group "><label for="form2-email">Kolejność</label><input name="blocks[' + numItems + '][2][sequence]" class="form-control" id="form2-email" type="text" value="' + numItems + '"></div><div class="form-group "><label for="form2-email">Nazwa przycisku</label><input name="blocks[' + numItems + '][2][parametrs][action][text]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group "><label for="form2-email">Nazwa bloku</label><input name="blocks[' + numItems + '][2][parametrs][action][value]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group "><label for="form2-email">Opóźnienie w sekundach</label><input name="blocks[' + numItems + '][2][parametrs][delay]" class="form-control" id="form2-email" type="text" value=""></div></div><!-- /.panel-body --></div>';
        $('#buttonsContainer').append(template);
    });
    $('.add-switchToBlock').click(function () {
        var numItems = $('#switchToBlockContainer .item').length+1;
        var template = '<div class="panel item"><div class="panel-heading no-padding panel-item-raised"><h3>Przycisk</h3></div><div class="panel-highlight-action"><button class="btn btn-round btn-default btn-lg delete-block-entity"><i class="fa fa-trash"></i></button></div><div class="panel-body"><input name="blocks[' + numItems + '][3][type]" class="form-control" id="form2-email" type="hidden" value="3"><div class="form-group "><label for="form2-email">Kolejność</label><input name="blocks[' + numItems + '][3][sequence]" class="form-control" id="form2-email" type="text" value="' + numItems + '"></div><div class="form-group "><label for="form2-email">Nazwa bloku</label><input name="blocks[' + numItems + '][3][parametrs][action][value]" class="form-control" id="form2-email" type="text" value=""></div></div><!-- /.panel-body --></div>';
        $('#switchToBlockContainer').append(template);
    });
    $('.add-input').click(function () {
        var numItems = $('#messagesContainer .item').length+1;
        var template = '<div class="panel item"><div class="panel-heading no-padding panel-item-raised"><h3>Input</h3></div><div class="panel-highlight-action"><button class="btn btn-round btn-default btn-lg delete-block-entity"><i class="fa fa-trash"></i></button></div><div class="panel-body"><input name="blocks[' + numItems + '][4][type]" class="form-control" id="form2-email" type="hidden" value="4"><div class="form-group "><label for="form2-email">Kolejność</label><input name="blocks[' + numItems + '][4][sequence]" class="form-control" id="form2-email" type="text" value="' + numItems + '"></div><div class="form-group "><label for="form2-email">Subtype</label><input name="blocks[' + numItems + '][4][parametrs][action][sub_type]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group "><label for="form2-email">Placeholder</label><input name="blocks[' + numItems + '][4][parametrs][action][placeholder]" class="form-control" id="form2-email" type="text" value=""></div></div><!-- /.panel-body --></div>';
        $('#messagesContainer').append(template);
    });
    $('.add-link').click(function () {
        var numItems = $('#messagesContainer .item').length+1;
        var template = '<div class="panel item"><div class="panel-heading no-padding panel-item-raised"><h3>Link</h3></div><div class="panel-highlight-action"><button class="btn btn-round btn-default btn-lg delete-block-entity"><i class="fa fa-trash"></i></button></div><div class="panel-body"><input name="blocks[' + numItems + '][5][type]" class="form-control" id="form2-email" type="hidden" value="5"><div class="form-group "><label for="form2-email">Kolejność</label><input name="blocks[' + numItems + '][5][sequence]" class="form-control" id="form2-email" type="text" value="' + numItems + '"></div><div class="form-group "><label for="form2-email">Href</label><input name="blocks[' + numItems + '][5][parametrs][href]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group "><label for="form2-email">Text</label><input name="blocks[' + numItems + '][5][parametrs][text]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group checkbox"><label  class=""><input name="blocks[' + numItems + '][5][parametrs][blank]" type="checkbox" class="bootstrap-switch" checked><span class="label-text">Blank?</span></label></div><div class="form-group "><label for="form2-email">Opóźnienie w sekundach</label><input name="blocks[' + numItems + '][5][parametrs][delay]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group checkbox"><label  class=""><input name="blocks[' + numItems + '][5][parametrs][loading]" type="checkbox" class="bootstrap-switch" checked><span class="label-text">Pokaż "Pisze.."</span></label></div></div><!-- /.panel-body --></div>';
        $('#messagesContainer').append(template);
    });
    $('.add-image').click(function () {
        var numItems = $('#messagesContainer .item').length+1;
        var template = '<div class="panel item"><div class="panel-heading no-padding panel-item-raised"><h3>Image</h3></div><div class="panel-highlight-action"><button class="btn btn-round btn-default btn-lg delete-block-entity"><i class="fa fa-trash"></i></button></div><div class="panel-body"><input name="blocks[' + numItems + '][6][type]" class="form-control" id="form2-email" type="hidden" value="6"><div class="form-group "><label for="form2-email">Kolejność</label><input name="blocks[' + numItems + '][6][sequence]" class="form-control" id="form2-email" type="text" value="' + numItems + '"></div><div class="form-group "><label for="form2-email">Alt</label><input name="blocks[' + numItems + '][6][parametrs][alt]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group "><label for="form2-email">Src</label><input name="blocks[' + numItems + '][6][parametrs][src]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group "><label for="form2-email">Opóźnienie w sekundach</label><input name="blocks[' + numItems + '][6][parametrs][delay]" class="form-control" id="form2-email" type="text" value=""></div><div class="form-group checkbox"><label  class=""><input name="blocks[' + numItems + '][6][parametrs][loading]" type="checkbox" class="bootstrap-switch" checked><span class="label-text">Pokaż "Pisze.."</span></label></div></div><!-- /.panel-body --></div>';
        $('#messagesContainer').append(template);
    });

    $('#block').on('click', '.delete-block-entity', function () {
        $(this).parent().parent().remove();
    });
})
</script>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
@endsection
