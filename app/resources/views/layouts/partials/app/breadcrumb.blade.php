<ol class="breadcrumb">
    @foreach ($breadcrumb as $breadcrumbItem)
    <li class="{{ $breadcrumbItem['class'] }}">
        <a href="{{ $breadcrumbItem['href'] }}">
            {{ $breadcrumbItem['value'] }}
        </a>
    </li>
    @endforeach
</ol>