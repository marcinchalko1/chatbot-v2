<div class="sidebar sidebar-left sidebar-dark sidebar-fixed sidebar-navbar-theme" id="sidebar">
    <div class="sidebar-scrollable-content">
        <div class="sidebar-body">
            <div class="sidebar-cover">
                <a class="sidebar-user" data-toggle="collapse" href="#sidebar-highlight" aria-expanded="false" aria-controls="sidebar-highlight">
                    <div class="sidebar-user-img">
                        <img src="http://via.placeholder.com/500x500" alt="" class="img-circle img-online img-thumbnailimg-thumbnail-primary">
                    </div>
                    <div class="sidebar-user-name">
                        {{ Auth::user()->name }}
                        <span class="sidebar-user-expand"><i class="fa fa-caret-down"></i></span>
                        <span class="text-small sidebar-user-email">
                            {{ Auth::user()->email }}
                        </span>
                    </div>
                </a>
                <div class="sidebar-highlight collapse" id="sidebar-highlight">
                    <ul class="main-nav">
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="mdi mdi-logout"></i> Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-menu-container">
                <ul class="main-nav" id="main-nav">
                    <li class="main-nav-label">
                        <span>
                            Main Navigation
                        </span>
                    </li>
                    <li>
                        <a href="{{ url('/home') }}">
                            <i class="mdi mdi-cube"></i>
                            <span class="title">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/chats') }}">
                            <i class="mdi mdi-message"></i>
                            <span class="title">Chats</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.main-menu-container -->
        </div>
        <!-- /.sidebar-body -->
    </div>
    <!-- /.sidebar-scrollable-content -->

    <div class="sidebar-footer">
        <div class="horizontal-nav">
            <ul class="horizontal-nav horizontal-nav-3">
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="mdi mdi-logout"></i>
                    </a>
                </li>
                <li>
                    <a target="_blank" href="{{ url('/') }}">
                        <i class="mdi mdi-earth"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>