<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- CSRF Token --> 
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Website title -->
        <title>{{ config('app.name', 'Chatbot') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CRubik:300,400,500%7CSource+Code+Pro">

        <!-- Preloader -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/loaders.css/loaders.min.css') }}">

        <style>
            #loader-container {
                display: block;
                position: fixed;
                z-index: 100;
                left: 0;
                top:0;
                width: 100%;
                height: 100%;
                width: 100vw;
                height: 100vh;
                background: rgb(241,246,251);
                overflow: hidden;
            }
            #loader-container .loader-inner {
                text-align: center;
                position: absolute;
                top:40%;
                left:0;
                width: 100%;
                top:40vh;
                left:0;
                width: 100vw;
                transform: scale(1);

            }
            #loader-container .loader-inner div {
                background-color: #7359EE;
            }
        </style>

        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/c3/c3.min.css') }}">

        <!--  Bootstrap -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/bootstrap/dist/css/bootstrap.min.css') }}">

        <!-- Font Awesome Icon Fonts -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/font-awesome/css/font-awesome.min.css') }}">

        <!-- Include MDI Icon Fonts -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/mdi/css/materialdesignicons.min.css') }}">

        <!-- Open Iconic Icon Fonts -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/open-iconic/font/css/open-iconic-bootstrap.min.css') }}">

        <!-- Bootstrap Switch -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}">

        <!-- Chartist Charts base CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/chartist/dist/chartist.min.css') }}">

        <!-- Toastr Popups -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/toastr/toastr.min.css') }}">

        <!-- Sweet Alert Popups -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/sweetalert2/dist/sweetalert2.min.css') }}">

        <!-- iCheck Checkboxes -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/iCheck/skins/square/_all.css') }}">

        <!-- Emphasize (Always Include Last)-->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/css/style.css') }}">
        
        <!-- Datepicker -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
        
        <style>
            .subtasks h4 {
                margin-bottom: 1em;
            }
            .subtask h6 {
                margin-bottom: 1em;
            }
        </style>
        
        <!-- jQuery -->
        <script src="{{ asset('app/vendor/jquery/dist/jquery.min.js') }}"></script>

    </head>
    <body class="sidebar-toggled-mini">        

        @yield('content')

        <!-- Bootstrap Javascript -->
        <script src="{{ asset('app/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

        <!-- Chartjs-->
        <script src="{{ asset('app/vendor/chart.js/dist/Chart.min.js') }}"></script>

        <!-- Easy Pie Chart-->
        <script src="{{ asset('app/vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>

        <!-- Bootstrap Switch-->
        <script src="{{ asset('app/vendor/bootstrap-switch/dist/js/bootstrap-switch.min.js') }}"></script>

        <!-- Color Variables -->
        <script src="{{ asset('app/scripts/colors.min.js') }}"></script>

        <!-- Collapsible Menu Plugin -->
        <script src="{{ asset('app/scripts/collapsibleMenu.min.js') }}"></script>

        <!-- Collapsible Sidebar Plugin -->
        <script src="{{ asset('app/scripts/sidebar.js') }}"></script>

        <!-- Panel Actions -->
        <script src="{{ asset('app/scripts/panel.min.js') }}"></script>

        <!-- Class Toggle Plugin -->
        <script src="{{ asset('app/scripts/classtoggle.min.js') }}"></script>

        <!-- Initialize Emphasize -->
        <script src="{{ asset('app/scripts/init.js') }}"></script>

        <!-- Chartist -->
        <script src="{{ asset('app/vendor/chartist/dist/chartist.min.js') }}"></script>

        <!-- jQuery SlimScroll -->
        <script src="{{ asset('app/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

        <!-- Sweet Alert -->
        <script src="{{ asset('app/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>

        <!-- Toastr -->
        <script src="{{ asset('app/vendor/toastr/toastr.min.js') }}"></script>

        <!-- Icheck Checkboxes -->
        <script src="{{ asset('app/vendor/iCheck/icheck.min.js') }}"></script>
        <script src="{{ asset('app/vendor/d3/d3.min.js') }}"></script>
        <script src="{{ asset('app/vendor/c3/c3.min.js') }}"></script>
        <script src="{{ asset('app/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        
        <script>
            $(function(){
                $('.datepicker-input').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    maxDate: '{{ date("Y-m-d") }}'
                });
                $('.datepicker-input-group').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    maxDate: '{{ date("Y-m-d") }}'
                });
            });
        </script>
        
        <script>
                $(function () {
                    var ctx = $('#chart-line');
                    var salesChart = new Chart(ctx, {
                        'type': 'line',
                        data: {
                            labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                            datasets: [
                                {
                                    label: 'Performance Score',
                                    data: [200, 250, 340, 160, 150, 170, 200, 180, 210, 150, 200, 290, 260, 330, 350, 300, 340, 400, 500, 600],
                                    borderColor: "rgba(119,95,238,1)",
                                    backgroundColor: "rgba(119,95,238,1)",
                                    lineTension: 0,
                                    pointRadius: 0

                                }
                            ],

                        },
                        options: {
                            maintainAspectRatio: false,
                            scales: {
                                yAxes: [{
                                        gridLines: {
                                            tickMarkLength: 0,
                                            drawBorder: false,
                                            color: 'rgba(240,240,240,1)',
                                            zeroLineColor: 'rgba(240,240,240,1)',
                                            display: false
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            autoSkip: false,
                                            display: false,
                                            stepSize: 50,

                                        }
                                    }],
                                xAxes: [{
                                        gridLines: {
                                            display: false,
                                            tickMarkLength: 0,
                                            drawBorder: false,
                                            color: 'rgba(240,240,240,1)',
                                            zeroLineColor: 'rgba(240,240,240,1)',
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            display: false,

                                        }

                                    }]
                            },
                            legend: {
                                display: false,
                                position: 'top',
                                fullWidth: false,
                                labels: {
                                    boxWidth: 14
                                }

                            }
                        }
                    })

                    var ctx = $('#chart-line2');
                    var salesChart = new Chart(ctx, {
                        'type': 'line',
                        data: {
                            labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "1", "2", "3", "4", ],
                            datasets: [
                                {
                                    label: 'Performance Score',
                                    data: [200, 250, 340, 210, 250, 200, 400, 500, 400, 290, 260, 340, 330, 350, 300],
                                    borderColor: "rgba(30,126,239,0.9)",
                                    backgroundColor: "rgba(30,126,239,0.9)",
                                    lineTension: 0,
                                    pointRadius: 0,

                                }
                            ],

                        },
                        options: {
                            maintainAspectRatio: false,
                            scales: {
                                yAxes: [{
                                        gridLines: {
                                            tickMarkLength: 0,
                                            drawBorder: false,
                                            color: 'rgba(240,240,240,1)',
                                            zeroLineColor: 'rgba(240,240,240,1)',
                                            display: false
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            autoSkip: false,
                                            display: false,
                                            stepSize: 50,

                                        }
                                    }],
                                xAxes: [{
                                        gridLines: {
                                            display: false,
                                            tickMarkLength: 0,
                                            drawBorder: false,
                                            color: 'rgba(240,240,240,1)',
                                            zeroLineColor: 'rgba(240,240,240,1)',
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            display: false,

                                        }

                                    }]
                            },
                            legend: {
                                display: false,
                                position: 'top',
                                fullWidth: false,
                                labels: {
                                    boxWidth: 14
                                }

                            }
                        }
                    })

                    var ctx = $('#chart-line3');
                    var salesChart = new Chart(ctx, {
                        'type': 'line',
                        data: {
                            labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "1", "2", "3", "4", ],
                            datasets: [
                                {
                                    label: 'Performance Score',
                                    data: [200, 250, 240, 250, 220, 200, 300, 400, 300, 190, 160, 240, 330, 350, 300],
                                    borderColor: "rgba(38,193,177,0.9)",
                                    backgroundColor: "rgba(38,193,177,0.9)",
                                    lineTension: 0,
                                    pointRadius: 0,

                                }
                            ],

                        },
                        options: {
                            maintainAspectRatio: false,
                            scales: {
                                yAxes: [{
                                        gridLines: {
                                            tickMarkLength: 0,
                                            drawBorder: false,
                                            color: 'rgba(240,240,240,1)',
                                            zeroLineColor: 'rgba(240,240,240,1)',
                                            display: false
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            autoSkip: false,
                                            display: false,
                                            stepSize: 50,

                                        }
                                    }],
                                xAxes: [{
                                        gridLines: {
                                            display: false,
                                            tickMarkLength: 0,
                                            drawBorder: false,
                                            color: 'rgba(240,240,240,1)',
                                            zeroLineColor: 'rgba(240,240,240,1)',
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            display: false,

                                        }

                                    }]
                            },
                            legend: {
                                display: false,
                                position: 'top',
                                fullWidth: false,
                                labels: {
                                    boxWidth: 14
                                }

                            }
                        }
                    })

                    var ctx = $('#chart-bar');
                    var salesChart = new Chart(ctx, {
                        'type': 'bar',
                        data: {
                            labels: ["Jan", "Feb", "March", "April", "May"],
                            datasets: [
                                {
                                    label: 'Performance Score',
                                    data: [100, 50, 180, 70, 80],
                                    borderColor: "rgba(38,193,177,1)",
                                    backgroundColor: "rgba(38,193,177,1)",

                                    lineTension: 0.3,
                                    borderWidth: 1,

                                },

                                {
                                    label: 'Performance Score',
                                    data: [200, 100, 180, 50, 90],
                                    borderColor: "rgba(30,126,239,1)",
                                    backgroundColor: "rgba(30,126,239,1)",
                                    lineTension: 0.3,
                                    borderWidth: 1,

                                },
                                {
                                    label: 'Performance Score',
                                    data: [400, 150, 280, 130, 300],
                                    borderColor: 'rgba(115,89,238,1)',
                                    backgroundColor: "rgba(115,89,238,1)",

                                    lineTension: 0.3,
                                    borderWidth: 1,

                                },
                            ],

                        },
                        options: {

                            maintainAspectRatio: false,

                            scales: {
                                yAxes: [{
                                        gridLines: {
                                            tickMarkLength: 0,
                                            drawBorder: false,
                                            color: 'rgba(240,240,240,1)',
                                            zeroLineColor: 'rgba(240,240,240,1)',
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            autoSkip: true,
                                            stepSize: 100,
                                            display: true,
                                            padding: 10,

                                        }
                                    }],
                                xAxes: [{
                                        categoryPercentage: 0.7,
                                        gridLines: {
                                            tickMarkLength: 10,
                                            drawBorder: false,
                                            color: 'rgba(240,240,240,0)',
                                            zeroLineColor: 'rgba(240,240,240,1)',
                                            display: true,
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            display: true,

                                        }

                                    }]
                            },
                            legend: {
                                display: false,

                            }
                        }
                    })

                    var chart = c3.generate({
                        bindto: '#donut-chart',
                        data: {
                            columns: [
                                ['Retweets', 0],
                                ['Shares', 100],
                                ['Sale', 160],
                                ['Revenue', 80],
                            ],
                            colors: {
                                'Retweets': '#FC5F7C',
                                'Shares': '#1E88E5',
                                'Sale': '#7359EE',
                                'Revenue': 'rgba(38,193,177,0.9)',

                            },
                            type: 'donut'
                        }
                    });


                    $('.easyPieChart').easyPieChart({
                        size: 28,
                        scaleColor: false,
                        lineWidth: 2,
                        barColor: '#fff',
                        trackColor: 'rgba(0,0,0,0.1)',
                    });
                });
        </script>

    </body>
</html>
