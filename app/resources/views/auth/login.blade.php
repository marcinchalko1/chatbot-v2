@extends('layouts.site')

@section('content')
<div class="page-wrapper">

    <div class="page full-width">

        <div class="page-content">
            <div class="container-fluid">
                <div class="fullpage-form-container">
                    <div class="panel panel-primary panel-form">
                        <div class="panel-heading" style="padding-top:2.5em; padding-bottom:2.5em">
                            <h2 class="align-center bold" style="font-weight:400">Emphasize Admin</h2>
                        </div>

                        <div class="panel-body">
                            <h4 class="align-center form-heading">Login to Continue</h4>
                            <form class="mdform" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group md-form-group">
                                    <label for="form1-email">Email</label>
                                    <input type="email" class="form-control" id="form1-email" name="email" value="{{ old('email') }}" required>
                                </div>
                                @if ($errors->has('email'))
                                <div class="alert alert-bold alert-danger" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                                @endif
                                <div class="form-group md-form-group">
                                    <label for="form1-pass">Password</label>
                                    <input type="password" class="form-control" id="form1-pass" name="password" required>
                                </div>
                                @if ($errors->has('password'))
                                <div class="alert alert-bold alert-danger" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                                @endif
                                <div class="form-group checkbox">
                                    <label for="form1-check" class="">
                                        <input type="checkbox" id="form1-check" class="icheck" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                                               <span class="label-text">Keep me logged in</span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">
                                    LOGIN
                                </button>
                                <div class="form-group form-link">
                                    <p class="">
                                        <a href="{{ route('password.request') }}">
                                            Forgot Password?
                                        </a>
                                    </p>
                                </div>
                            </form>
                        </div>
                        
                        <div class="panel-footer">
                            <p class="help-block align-center">
                                Don't have an account? 
                                <a href="{{ route('register') }}">
                                    Signup, it's free!
                                </a>
                            </p>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
</div>
@endsection
