@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <div class="panel">
                <div class="panel-heading no-padding panel-item-raised">
                    <img src="http://via.placeholder.com/1000x665" alt="" class="panel-image">
                </div>
                <div class="panel-highlight-action">
                    <a href="{{ url('/templates/create') }}">
                        <button class="btn btn-round btn-danger btn-lg">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
                </div>
                <div class="panel-body">
                    <h3>Utwórz nowy szablon</h3>
                    <p>
                        Wybierz tę opcję jeśli chcesz utworzyć nowy scenariusz
                    </p>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        
        @foreach ($templates as $templatesKey => $templatesItem)
            @php
                $class = ($templatesItem->public) ? 'success' : 'default';
            @endphp
            <div class="col-lg-3">
                <div class="panel panel-raised">
                    <div class="panel-heading no-padding panel-item-raised">
                        <img src="http://via.placeholder.com/1000x665" alt="" class="panel-image">
                    </div>
                    <div class="panel-body align-center">
                        <h2 style="margin-bottom:0.7rem">{{ $templatesItem->name }}</h2>
                        <h4 class="" style="margin-bottom:1.5rem">
                            0 czatów
                        </h4>
                        <p>
                            <a href="public/{{ $templatesItem->template_id }}">
                                <button class="btn btn-{{ $class }} btn-round ">
                                    <i class="fa fa-globe"></i>
                                </button>
                            </a>
                            <a href="templates/form/{{ $templatesItem->template_id }}">
                                <button class="btn btn-info btn-round ">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </a>
                            <a href="templates/delete/{{ $templatesItem->template_id }}">
                                <button class="btn btn-warning btn-round ">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </a>
                        </p>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        @endforeach
        
    </div>
</div>
@endsection
