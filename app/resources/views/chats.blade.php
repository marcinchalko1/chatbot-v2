@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body no-padding no-padding-v">
                    <div class="media align-middle">
                        <div class="media-left no-padding media-primary">
                            <div class="square-icon square-icon-primary square-icon-xlg">
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="align-right" style="padding:1.5em;">
                                <h6>KAMPANIE</h6>
                                <h2 class="no-margin">
                                    1
                                </h2>
                            </div>
                        </div>
                        <!-- /.media-body -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body no-padding no-padding-v">
                    <div class="media align-middle">
                        <div class="media-left no-padding media-info">
                            <div class="square-icon square-icon-info square-icon-xlg">
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="align-right" style="padding:1.5em;">
                                <h6>UŻYTKOWNIKÓW</h6>
                                <h2 class="no-margin">
                                    {{ $leads }}
                                </h2>
                            </div>
                        </div>
                        <!-- /.media-body -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body no-padding no-padding-v">
                    <div class="media align-middle">
                        <div class="media-left no-padding media-danger">
                            <div class="square-icon square-icon-danger square-icon-xlg">
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="align-right" style="padding:1.5em;">
                                <h6>SESJI</h6>
                                <h2 class="no-margin">
                                    {{ $conversations }}
                                </h2>
                            </div>
                        </div>
                        <!-- /.media-body -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body no-padding no-padding-v">
                    <div class="media align-middle">
                        <div class="media-left no-padding media-primary">
                            <div class="square-icon square-icon-primary square-icon-xlg">
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="align-right" style="padding:1.5em;">
                                <h6>KONWERSACJI</h6>
                                <h2 class="no-margin">
                                    {{ $countConversationsWithAnswers }}
                                </h2>
                            </div>
                        </div>
                        <!-- /.media-body -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body no-padding no-padding-v">
                    <div class="media align-middle">
                        <div class="media-left no-padding media-info">
                            <div class="square-icon square-icon-info square-icon-xlg">
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="align-right" style="padding:1.5em;">
                                <h6>ODPOWIEDZI NA "TAK"</h6>
                                <h2 class="no-margin">
                                    {{ $getConversationsOnTrue }}
                                </h2>
                            </div>
                        </div>
                        <!-- /.media-body -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body no-padding no-padding-v">
                    <div class="media align-middle">
                        <div class="media-left no-padding media-danger">
                            <div class="square-icon square-icon-danger square-icon-xlg">
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="align-right" style="padding:1.5em;">
                                <h6>ODPOWIEDZI NA "NIE"</h6>
                                <h2 class="no-margin">
                                    {{ $getConversationsOnFalse }}
                                </h2>
                            </div>
                        </div>
                        <!-- /.media-body -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->
    <div class="row">

        <!-- /.col-lg-4 -->
        <div class="col-lg-12">

            <div class="panel">
                <div class="panel-heading">
                    <h2>
                        Konwersacje
                    </h2>
<!--                                <span class="subtext">
                        From July to November 2017
                    </span>-->
                    <ul class="panel-actions">
                        <li>
                            <a href="#" class="panel-action">
                                <i class="mdi mdi-dots-vertical">

                                </i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="panel-action panel-collapse-toggle">
                                <i class="mdi mdi-chevron-up">

                                </i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="panel-action panel-action-fullscreen ">
                                <i class="mdi mdi-fullscreen"></i>
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="panel-body" style="padding-top:0.5em;">
                    <p>&nbsp;</p>
                    {{ Form::open(array('url' => 'home', 'method' => 'get')) }}
                    <div class="form-group">
                        <div class='col-sm-5'>
                            <div class="input-group datepicker-input-group date" data-toggle="tooltip" title="Data początkowa">
                                <input value="{{ $filter['dateFrom'] }}" name="dateFrom" type='text' class="form-control" id="datetimepickerFrom"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class='col-sm-5'>
                            <div class="input-group datepicker-input-group date" data-toggle="tooltip" title="Data końcowa">
                                <input value="{{ $filter['dateTo'] }}" name="dateTo" type='text' class="form-control" id="datetimepickerTo"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class='col-sm-2'>
                            <div class="input-group" data-toggle="tooltip" title="Filtruj konwersacje po zakresie dat">
                                <input type='submit' class="form-control" value="Filtruj"/>
                            </div>
                        </div>
                        {{ Form::close() }}
                        <p>&nbsp;</p>

                        <table class="table">
                            <thead>
                            <th>Lp.</th>
                            <th>Użytkownik</th>
                            <th>Sesja</th>
                            <th>Data rozpoczęcia</th>
                            <th>Konwersacja</th>
                            </thead>
                            <tbody>
                                @foreach ($getConversationsWithAnswers as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $item->leadId }}</td>
                                    <td>{{ $item->conversationId }}</td>
                                    <td>{{ date("Y-m-d H:i:s", substr(round($item->conversationId, -3), 0 , 10)) }}</td>
                                    <td><a class="btn btn-info" href="{{ url('/home/messages/leadId/'.$item->leadId.'/conversationId/'.$item->conversationId) }}">Podgląd</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $getConversationsWithAnswers->links() }}

                        <p>&nbsp;</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
