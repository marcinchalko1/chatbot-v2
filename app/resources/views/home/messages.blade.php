@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Szczegóły rozmowy</div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    
                    <p>&nbsp;</p>

                    <table class="table">
                        <thead>
                        <th>Lp.</th>
                        <th>Użytkownik</th>
                        <th>Konwersacja</th>
                        <th>Wiadomość</th>
                        </thead>
                        <tbody>
                            @foreach ($messages as $key => $item)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $item->leadId }}</td>
                                <td>{{ $item->conversationId }}</td>
                                <td>{{ $item->content }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <p>&nbsp;</p>

                    <a class="btn btn-success" href="{{ url('home') }}">Powrót</a>

                    <p>&nbsp;</p>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
