//const CLASS_CIRCLE = '.circle';
//const CLASS_ICON = '.icon-elements';
//const CLASS_MODAL = '.modal-wrapper';
//const CLASS_ICON_ACTIVE = 'js-icon-active';
//const CLASS_MODAL_ACTIVE = 'js-modal-active';
//
//const elementCircle = document.querySelector(CLASS_CIRCLE);
//const elementIcon = document.querySelector(CLASS_ICON);
//const elementModal = document.querySelector(CLASS_MODAL);
//const elementInput = document.querySelector('#myInput');

//const triggerAnimation = () => {
//    const isActive = elementIcon.classList.contains(CLASS_ICON_ACTIVE);
//    console.log(isActive);
//    isActive ? (
//            elementIcon.classList.remove(CLASS_ICON_ACTIVE),
//            elementModal.classList.remove(CLASS_MODAL_ACTIVE)
//            ) : (
//            elementIcon.classList.add(CLASS_ICON_ACTIVE),
//            elementModal.classList.add(CLASS_MODAL_ACTIVE)
//            );
//};

//elementCircle.addEventListener('click', () => triggerAnimation());

var displayCounter = true;
var displaySound = true;
var showChatSince = 10000;

$('.botui-container').on("DOMSubtreeModified", function () {
//    $('#bot').scrollTop($('#bot').scrollHeight);
//    window.scrollTo(0,document.querySelector(".popup-messages").scrollHeight);

//    $(".popup-messages").mCustomScrollbar("scrollTo","bottom");
    
    $('.messages-counter').html($('.botui-message').length);
    
    if(displayCounter) {
        $('.messages-counter').show();
        $('.messages-counter').html($('.botui-message').length);
    }  
    
    if(displaySound) {
        $("#mysoundclip")[0].play();
    }
});
 
if(showChatSince) {
    function timer() {
        if(!$('#popup').hasClass('in')) {
            $('.chat-header-button').click();
            $("#mysoundclip")[0].play();
        }
    }
    window.setTimeout(timer, showChatSince);//2000);
}

