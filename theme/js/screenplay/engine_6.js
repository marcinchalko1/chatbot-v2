var botui = new BotUI('bot'); // give it the id of container

// Tematy chatbota

var topics = [
    {
        text: 'Oferta sklepu',
        value: 'oferta'
    }, {
        text: 'Aktualne promocje',
        value: 'promocje'
    }, {
        text: 'Opcje wysyłki',
        value: 'wysylka'
    }, {
        text: 'Kody rabatowe',
        value: 'rabaty'
    }, {
        text: 'Zasada zwrotów',
        value: 'zwroty'
    }, {
        text: 'Dane kontaktowe',
        value: 'kontakt'
    },
];

// odpowiedzi na tematy chatbota

var oferta = function () {
    botui.message.add({// let user choose something
        delay: 300,
        content: 'odpowiedź na ofertę'
    });
    botui.message.add({// let user choose something
        delay: 300,
        content: 'plus extra post'
    });
};

var promocje = function () {
    return botui.message.add({// let user choose something
        delay: 300,
        content: 'odpowiedź na promocje'
    });
};

var wysylka = function () {
    return botui.message.add({// let user choose something
        delay: 300,
        content: 'odpowiedź na wysylka'
    });
};

var rabaty = function () {
    return botui.message.add({// let user choose something
        delay: 300,
        content: 'odpowiedź na rabaty'
    });
};

var zwroty = function () {
    return botui.message.add({// let user choose something
        delay: 300,
        content: 'odpowiedź na zwroty'
    });
};

var kontakt = function () {
    return botui.message.add({// let user choose something
        delay: 300,
        content: 'odpowiedź na kontakt'
    });
};

// Mechanizm uruchamijący pytania

var ask = function () {
    botui.message.add({
        delay: 500,
        content: 'Jak możemy pomóc? Wybierz temat, który Cię interesuje:'
    }).then(function () {
        return botui.action.button({// let user choose something
            delay: 300,
            action: topics
        });
    }).then(function (res) {
        switch (res.value) {
            case 'oferta':
                oferta();
                break;
            case 'promocje':
                promocje();
                break;
            case 'wysylka':
                wysylka();
                break;
            case 'rabaty':
                rabaty();
                break;
            case 'zwroty':
                zwroty();
                break;
            case 'kontakt':
                kontakt();
                break;
        }
    }).then(function () {
        more();
    });
};

// Zakończenie rozmowy

var end = function () {
    botui.message.add({
        delay: 1000,
        content: 'Mamy nadzieję, że uzyskałeś pomoc. Jeśli będziesz miał problem wróc do bota.'
    });
    
};

// Pytanie o kontynuację rozmowy z chatbotem

var more = function () {
    return botui.message.add({
        delay: 500,
        content: 'Czy chcesz nas o coś jeszcze zapytać?'
    }).then(function () {
        return botui.action.button({
            delay: 1000,
            action: [{
                    text: 'Tak',
                    value: 'true'
                }, {
                    text: 'Nie',
                    value: 'false'
                }]
        });
    }).then(function (res) {
        switch (res.value) {
            case 'false':
                end();
                break;
            case 'true':
                ask();
                break;
        }
    });
};

// SCENARIUSZ

botui.message.add({// show first message
    delay: 500,
    content: 'Cześć!',
    loading: true // fake typing
}).then(function () {
    $("button").click(function(){
        $.ajax({url: "demo_test.txt", success: function(result){
            $("#div1").html(result);
        }});
    });
    return botui.message.add({// second one
        delay: 500,
        loading: true,
        content: 'Aby usprawnić komunikację z naszym sklepem przygotowaliśmy dla Ciebie bota.'
    });
}).then(function () {
    return botui.message.add({// second one
        delay: 500,
        loading: true,
        content: 'Mamy nadzieję, że pomoże Ci znaleźć odpowiednie informacje.'
    });
}).then(function () {
    return botui.message.add({// second one
        delay: 500,
        loading: true,
        content: 'Czy chciałbyś zapisać się na nasz newsletter?'
    });
}).then(function () {
    return botui.action.button({// let user choose something
        delay: 300,
        action: [{
                text: 'Tak',
                value: 'true'
            }, {
                text: 'Nie',
                value: 'false'
            }]
    });
}).then(function (res) {
    switch (res.value.toLowerCase()) {
        case 'true':
            return botui.message.add({
                delay: 500,
                loading: true,
                content: 'Podaj adres e-mail.'
            }).then(function () {
                return botui.action.text({
                    delay: 400,
                    action: {
                        size: 18,
                        icon: 'user-circle-o',
                        sub_type: 'text',
                        placeholder: 'np. imie.nazwisko#domena.pl'
                    }
                });
            }).then(function (res) {
                email = res.value; // save new value
                return botui.message.add({
                    delay: 300,
                    loading: true,
                    content: 'Doskonale! Od teraz otrzymasz na adres e-mail ' + email + ' informacje o najlepszych ofertach naszego sklepu!'
                });
            });
            break;
        case 'false':
            return botui.message.add({
                delay: 500,
                loading: true,
                content: 'Trudno, daj nam znać jeśli zmienisz zdanie.'
            });
            break;
    }
}).then(function () {
    ask();
});