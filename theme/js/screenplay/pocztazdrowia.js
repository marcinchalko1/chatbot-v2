var botui = new BotUI('bot'); // give it the id of container

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

$.get("http://ipinfo.io", function (response) {
    newIP = response.ip.split('.').join("");
    setCookie('adressIP', newIP);
}, "jsonp");

if (getCookie('leadId') === '') {
    setCookie('leadId', getCookie('adressIP') + Date.now(), 1);
}

setCookie('campaignId', '1', 1);
setCookie('conversationId', Date.now(), 1);

var register = function (id, content) {
    $.ajaxSetup({
        beforeSend: function (xhrObj) {
            xhrObj.setRequestHeader("Authorization", "Bearer WLonKahKVz5GbmSfgnniblkqRGYrYiwnMa8bHMoBPA60aYWGN2WI9F6fO61B");
            xhrObj.setRequestHeader("Accept", "application/json");
        }
    });

    var request = $.ajax({
        url: "https://chalko.eu/chatbot/app/public/api/messages/create",
        method: "POST",
        data: {
            leadId: getCookie('leadId'),
            campaignId: getCookie('campaignId'),
            conversationId: getCookie('conversationId'),
            count: id,
            content: content
        },
        dataType: "html"
    });

    request.done(function (msg) {
        console.log(msg);
    });

    request.fail(function (jqXHR, textStatus) {
        console.log(textStatus);
    });
};

function validateForm(email) {
    var retur = 'true';
    var x = email;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        retur = 'false';
    }
    return retur;
}

var policy1 = function () {
    return botui.message.add({
        delay: 3000,
        loading: true,
        content: 'Czy wyrażasz zgodę na przetwarzanie podanych powyżej danych w celu otrzymywania newslettera?'
    }).then(function () {
        return botui.action.button({// let user choose something
            delay: 1000,
            action: [{
                    text: 'Tak',
                    value: 'true'
                }, {
                    text: 'Nie',
                    value: 'false'
                }]
        });
    }).then(function (res) {
        switch (res.value.toLowerCase()) {
            case 'true':
                policy2();
                break;
            case 'false':
                return botui.message.add({
                    delay: 3000,
                    loading: true,
                    content: 'Musisz wyrazić zgodę w innym wypadku nie otrzymasz raportu. Czy chcesz kontunuować?'
                }).then(function () {
                    return botui.action.button({// let user choose something
                        delay: 1000,
                        action: [{
                                text: 'Tak',
                                value: 'true'
                            }, {
                                text: 'Nie',
                                value: 'false'
                            }]
                    }).then(function (res) {
                        switch (res.value.toLowerCase()) {
                            case 'true':
                                policy1();
                                break;
                            case 'false':
                                byebye();
                                break;
                        }
                    });
                });
                break;
        }
    });
};

var policy2 = function () {
    return botui.message.add({
        delay: 3000,
        loading: true,
        content: 'Czy wyrażasz zgodę na otrzymywanie informacji handlowych?'
    }).then(function () {
        return botui.action.button({// let user choose something
            delay: 1000,
            action: [{
                    text: 'Tak',
                    value: 'true'
                }, {
                    text: 'Nie',
                    value: 'false'
                }]
        });
    }).then(function (res) {
        switch (res.value.toLowerCase()) {
            case 'true':
                submitEmail();
                break;
            case 'false':
                return botui.message.add({
                    delay: 3000,
                    loading: true,
                    content: 'Musisz wyrazić zgodę w innym wypadku nie otrzymasz raportu. Czy chcesz kontunuować?'
                }).then(function () {
                    return botui.action.button({// let user choose something
                        delay: 1000,
                        action: [{
                                text: 'Tak',
                                value: 'true'
                            }, {
                                text: 'Nie',
                                value: 'false'
                            }]
                    }).then(function (res) {
                        switch (res.value.toLowerCase()) {
                            case 'true':
                                policy2();
                                break;
                            case 'false':
                                byebye();
                                break;
                        }
                    });
                });
                break;
        }
    });
};

var submitEmail = function () {
    return botui.message.add({
        delay: 3000,
        loading: true,
        content: 'Proszę o wpisanie w polu poniżej adresu e-mail, na który mam wysłać raport.'
    }).then(function () {
        return botui.action.text({
            delay: 1000,
            action: {
                size: '100',
                icon: 'user-circle-o',
                sub_type: 'text',
                placeholder: 'np. nazwa@domena.pl'
            }
        });
    }).then(function (res) {

        var email = res.value.toLowerCase();
        var isValidEmail = validateForm(email);

        switch (isValidEmail) {
            case 'true':
                return botui.message.add({
                    delay: 3000,
                    loading: true,
                    content: 'Na adres e-mail ' + email + ' niedługo wyślę Twój bezpłatny raport.'
                }).then(function () {
                    return botui.message.add({
                        delay: 3000,
                        loading: true,
                        content: 'Wiadomość najprawdopodobniej trafi do głównej karty lub do karty z ofertami.'
                    });
                }).then(function () {
                    return botui.message.add({
                        delay: 3000,
                        loading: true,
                        content: 'W najgorszym przypadku proszę sprawdzić dział SPAM.'
                    });
                }).then(function () {
                    end();
                });
                break;
            case 'false':
                return botui.message.add({
                    delay: 3000,
                    loading: true,
                    content: 'Błędny adres e-mail. Czy chcesz wprowadzić poprawny adres e-mail?'
                }).then(function () {
                    return botui.action.button({// let user choose something
                        delay: 1000,
                        action: [{
                                text: 'Tak',
                                value: 'true'
                            }, {
                                text: 'Nie',
                                value: 'false'
                            }]
                    });
                }).then(function (res) {
                    switch (res.value.toLowerCase()) {
                        case 'true':
                            submitEmail();
                            break;
                        case 'false':
                            byebye();
                            break;
                    }
                });
                break;
        }
    });
};

var byebye = function () {
    return botui.message.add({
        delay: 3000,
        loading: true,
        content: 'W porządku. Życzę pożytecznie spędzonego czasu z artykułami Poczty Zdrowia.'
    }).then(function () {
        end();
    });
};

var end = function () {
    return botui.action.button({// let user choose something
        delay: 1000,
        action: [{
                text: 'Bardzo dziękuję!',
                value: 'true'
            }]
    });
};

// SCENARIUSZ

var screenplay = function () {
    botui.message.add({// show first message
        delay: 1000,
        loading: true // fake typing
    }).then(function (index) {
        var content = 'Witaj, mam dla Ceibie prezent, który możesz otrzymać zupełnie za darmo?';
        register(1, content);
        return botui.message.update(index, {// show first message
            content: content
        });
    }).then(function () {
        return botui.message.add({// show first message
            delay: 1000,
            loading: true // fake typing
        });
    }).then(function (index) {
        var content = 'Czy mogę kontynuować?';
        register(2, content);
        return botui.message.update(index, {// show first message
            content: content
        });
    }).then(function () {
        return botui.action.button({// let user choose something
            delay: 1000,
            action: [{
                    text: 'Tak',
                    value: 'true'
                }, {
                    text: 'Nie',
                    value: 'false'
                }]
        });
    }).then(function (res) {
        register(3, res.text);
        switch (res.value.toLowerCase()) {
            case 'true':
                return botui.message.add({
                    delay: 1000,
                    loading: true
                }).then(function (index) {
                    var content = 'Świetnie.';
                    register(3, content);
                    return botui.message.update(index, {
                        content: content
                    });
                }).then(function () {
                    return botui.message.add({
                        delay: 4000,
                        loading: true,
                        content: 'Przygotowaliśmy bezpłatny raport p.t. "10 największych kłamstw o odżywianiu" dotyczący mitów żywieniowych.'
                    });
                }).then(function () {
                    return botui.message.add({
                        delay: 2000,
                        loading: true,
                        content: 'Czy chcesz dowiedzieć się co zaweira raport?'
                    });
                }).then(function () {
                    return botui.action.button({// let user choose something
                        delay: 1000,
                        action: [{
                                text: 'Tak, poproszę',
                                value: 'true'
                            }, {
                                text: 'Nie, dziękuję',
                                value: 'false'
                            }]
                    });
                }).then(function (res) {
                    switch (res.value.toLowerCase()) {
                        case 'true':
                            return botui.message.add({
                                delay: 1000,
                                loading: true,
                                content: 'Z raportu dowiesz się m.in.:'
                            }).then(function () {
                                return botui.message.add({
                                    delay: 4000,
                                    loading: true,
                                    content: 'Czy Twoje śniadanie rzeczywiście da Ci energię na cały dzień?'
                                });
                            }).then(function () {
                                return botui.message.add({
                                    delay: 4000,
                                    loading: true,
                                    content: 'Co pomoże Ci kontrolować apetyt i ograniczyć podjadanie?'
                                });
                            }).then(function () {
                                return botui.message.add({
                                    delay: 4000,
                                    loading: true,
                                    content: 'a także jaki produkt jest najgorszym sładnikiem współczesnych diet.'
                                });
                            }).then(function () {
                                return botui.message.add({
                                    delay: 4000,
                                    loading: true,
                                    content: 'Czy chcesz pobrać poradnik "10 największych kłamstw o odżywianiu" zupełnie za darmo?'
                                });
                            }).then(function () {
                                return botui.action.button({// let user choose something
                                    delay: 1000,
                                    action: [{
                                            text: 'Tak, poproszę',
                                            value: 'true'
                                        }, {
                                            text: 'Nie, dziękuję',
                                            value: 'false'
                                        }]
                                });
                            }).then(function (res) {
                                switch (res.value.toLowerCase()) {
                                    case 'true':
                                        return botui.message.add({
                                            delay: 3000,
                                            loading: true,
                                            content: 'Twój prezent jest już gotowy do wysłania.'
                                        }).then(function () {
                                            return botui.message.add({
                                                delay: 3000,
                                                loading: true,
                                                content: '![product image](images/gifts.gif)'
                                            });
                                        }).then(function () {
                                            return botui.message.add({
                                                delay: 3000,
                                                loading: true,
                                                content: 'Raport zostanie wysłany w formie elektronicznej prosto na skrzynkę e-mail.'
                                            });
                                        }).then(function () {
                                            policy1();
                                        });
                                        break;
                                    case 'false':
                                        byebye();
                                        break;
                                }
                            });
                            break;
                        case 'false':
                            byebye();
                            break;
                    }
                });
                break;
            case 'false':
                byebye();
                break;
        }
    });
};

//register(0, '');

screenplay();